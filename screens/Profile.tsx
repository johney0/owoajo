import React, { useState } from "react";
import {
  StyleSheet,
  Image,
  Dimensions,
  ImageBackground,
  StatusBar,
  Platform,
} from "react-native";
import { View, Text, Button } from "../components/Themed";
import Touchable from "react-native-platform-touchable";
import Layout from "../constants/Layout";
import { Ionicons, AntDesign } from "@expo/vector-icons";
import FloatingLabelInput from "react-native-floating-label-input";
import { useSelector } from "react-redux";
import ProfileModal from "../components/ProfileModal";
import { useFocusEffect } from "@react-navigation/native";
import moment from "moment";
const { width, height } = Dimensions.get("screen");
interface ProfileProps {
  navigation: any;
}

const Profile: React.FC<ProfileProps> = ({ navigation }) => {
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [profileModal, setProfileModal] = useState(false);
  const { Profile }: any = useSelector(
    (state: { settings: object }) => state.settings
  );
  const { User }: any = useSelector((state: { auth: object }) => state.auth);
  const { fullName, avatar } = User;
  const {
    phone,
    email,
    donationAmount,
    totalAmountDonated,
    totalNumberOfDonations,
    gender,
    ethnicity,
    dob,
  } = Profile;

  useFocusEffect(
    React.useCallback(() => {
      StatusBar.setBarStyle("light-content");
      Platform.OS === "android" && StatusBar.setBackgroundColor("#14171a");
    }, [])
  );
  return (
    <ImageBackground
      source={require("../assets/images/fact.jpg")}
      style={{ width, height }}
    >
      <View
        backgroundColor="rgba(0,0,0,.75)"
        flex
        style={{
          paddingVertical: Layout["dim"](1, "h"),
        }}
      >
        <View
          height={10}
          row
          style={{
            alignItems: "flex-end",
            paddingHorizontal: Layout["dim"](8, "w"),
          }}
          space="between"
        >
          <Touchable onPress={() => navigation.toggleDrawer()}>
            <Ionicons
              name="ios-arrow-back"
              color="#fff"
              size={Layout["dim"](3, "h")}
            />
          </Touchable>
          <Touchable onPress={() => setProfileModal(true)}>
            <AntDesign color="#fff" name="edit" size={Layout["dim"](3, "h")} />
          </Touchable>
        </View>

        <View flex style={{}}>
          <View flex>
            <View flex row style={{ paddingHorizontal: Layout["dim"](8, "w") }}>
              <View flex={0.7} style={{ justifyContent: "center" }}>
                <Image
                  source={{ uri: avatar, cache: "force-cache" }}
                  style={{ width: 100, height: 100, borderRadius: 50 }}
                />
              </View>
              <View flex style={{ justifyContent: "center" }}>
                <Text color="#fff" size="small" style={{ paddingBottom: 10 }}>
                  {fullName}
                </Text>
                <Text color="#fff" size="tiny">
                  Ethnicity:{" "}
                  <Text color="#fff" font="rubikMedium">
                    {ethnicity}
                  </Text>
                </Text>
                <Text color="#fff" size="tiny">
                  Gender:{" "}
                  <Text color="#fff" font="rubikMedium">
                    {gender}
                  </Text>
                </Text>
                <Text color="#fff" size="tiny">
                  Dob:{" "}
                  <Text color="#fff" font="rubikMedium">
                    {dob && dob.length > 0
                      ? moment(dob).format("MMM Do YY")
                      : moment(Date.now()).format("MMM Do YY")}
                  </Text>
                </Text>
              </View>
            </View>
            <View
              flex={0.5}
              style={{ paddingHorizontal: Layout["dim"](8, "w") }}
            >
              <View flex row style={{ alignItems: "center" }}>
                <Ionicons
                  name="ios-call"
                  color="#fff"
                  size={22}
                  style={{ paddingRight: 15 }}
                />
                <Text color="#fff">{phone}</Text>
              </View>
              <View flex row style={{ alignItems: "center" }}>
                <Ionicons
                  name="ios-mail"
                  color="#fff"
                  size={22}
                  style={{ paddingRight: 15 }}
                />
                <Text color="#fff">{email}</Text>
              </View>
            </View>
            <View flex={0.5} row>
              <View
                style={{ borderRightWidth: 0.3, borderColor: "#fff" }}
                flex
                middle
              >
                <Text color="#fff" font="rubikMedium" size="big">
                  ${totalAmountDonated}
                </Text>
                <Text color="#fff" font="rubikMedium" size="tiny">
                  #YES
                </Text>
              </View>
              <View
                flex
                middle
                // style={{ borderRightWidth: 0.3, borderColor: "#fff" }}
              >
                <Text color="#fff" font="rubikMedium" size="big">
                  {totalNumberOfDonations}
                </Text>
                <Text color="#fff" font="rubikMedium" size="tiny">
                  STREAK
                </Text>
                <Text color="#fff" font="rubikMedium" size="tiny">
                  (# of yes in a row)
                </Text>
              </View>
              {/* <View flex middle>
                <Text color="#fff" font="rubikMedium" size="big">
                  ${donationAmount}
                </Text>
                <Text color="#fff" font="rubikMedium" size="tiny">
                  Donating
                </Text>
              </View> */}
            </View>
          </View>
          <View
            flex
            space="around"
            style={{ paddingHorizontal: Layout["dim"](8, "w") }}
          >
            <FloatingLabelInput
              isFocused
              label="Old Password"
              value={oldPassword}
              onChangeText={(value) => setOldPassword(value)}
              containerStyles={{
                borderWidth: 0,
                borderBottomWidth: 1,
                borderBottomColor: "#fff",
              }}
              labelStyles={{ color: "#fff" }}
              inputStyles={{ color: "#fff" }}
            />
            <FloatingLabelInput
              isFocused
              label="New Password"
              value={newPassword}
              onChangeText={(value) => setNewPassword(value)}
              containerStyles={{
                borderWidth: 0,
                borderBottomWidth: 1,
                borderBottomColor: "#fff",
              }}
              labelStyles={{ color: "#fff" }}
              inputStyles={{ color: "#fff" }}
            />
            <FloatingLabelInput
              isFocused
              label="Confirm New Password"
              value={confirmPassword}
              onChangeText={(value) => setConfirmPassword(value)}
              containerStyles={{
                borderWidth: 0,
                borderBottomWidth: 1,
                borderBottomColor: "#fff",
              }}
              labelStyles={{ color: "#fff" }}
              inputStyles={{ color: "#fff" }}
            />
            <Button color="#fff" round shadowless>
              <Text color="#14171a">Change Password</Text>
            </Button>
          </View>
        </View>
        <ProfileModal
          profileModal={profileModal}
          setProfileModal={(val) => setProfileModal(val)}
          Profile={Profile}
        />
      </View>
    </ImageBackground>
  );
};
const styles = StyleSheet.create({});
export default Profile;
