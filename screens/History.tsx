import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Platform,
  ScrollView,
  Image,
  StatusBar,
} from "react-native";
import { View, Text, useThemeColor, Button } from "../components/Themed";
import Constants from "expo-constants";
import Layout from "../constants/Layout";
import { Ionicons, AntDesign, FontAwesome } from "@expo/vector-icons";
import { useFocusEffect } from "@react-navigation/native";

import Touchable from "react-native-platform-touchable";
import AddCard from "./AddCard";
import { useSelector } from "react-redux";
import { isEmptyObj } from "../components/Functions";
import moment from "moment";
interface HistoryProps {
  navigation: any;
}

const History: React.FC<HistoryProps> = ({ navigation }) => {
  const [addingCard, setAddingCard] = useState(false);
  const settings: any = useSelector(
    (state: { settings: object }) => state.settings
  );
  const { donations, Profile } = settings;
  const { card } = Profile || {};
  const { brand, name, last4, expMonth, expYear } = card || {};

  useFocusEffect(
    React.useCallback(() => {
      StatusBar.setBarStyle("light-content");
      Platform.OS === "android" && StatusBar.setBackgroundColor("#14171a");
    }, [])
  );
  return (
    <View
      flex
      backgroundColor="#14171a"
      style={{
        paddingTop: Platform.OS === "android" ? Constants.statusBarHeight : 0,
      }}
    >
      <View
        height={10}
        row
        style={{
          paddingHorizontal: Layout["dim"](8, "w"),
        }}
      >
        <Touchable
          onPress={() => navigation.goBack()}
          style={{ justifyContent: "flex-end" }}
        >
          <View row center>
            <Ionicons name="ios-arrow-back" color="#fff" size={24} />
            <Text size="small" color="#fff" style={{ paddingLeft: 10 }}>
              Back
            </Text>
          </View>
        </Touchable>
        <View
          flex
          style={{ justifyContent: "flex-end", alignItems: "flex-end" }}
        >
          <View
            row
            height={4}
            width={18}
            style={{
              borderRadius: 20,
              paddingHorizontal: 10,
              alignItems: "center",
            }}
            backgroundColor="#F2EA27"
            space="between"
          >
            <AntDesign name="edit" size={Layout["dim"](2, "h")} />

            <Text size="tiny" font="rubikMedium">
              EDIT
            </Text>
          </View>
        </View>
      </View>
      <View flex={0.7} middle>
        <View
          style={{
            width: "80%",
            height: "60%",
            borderRadius: 20,
            borderColor: "#F2EA27",
            borderWidth: 1,
          }}
          middle
        >
          {card && !isEmptyObj(card) ? (
            <View
              flex
              style={{ alignSelf: "stretch", borderRadius: 20, padding: 20 }}
            >
              <View flex>
                <View flex row style={{ alignItems: "center" }}>
                  <View flex style={{ justifyContent: "center" }}>
                    <Text
                      color="grey"
                      style={{
                        fontFamily: "rubikMedium",
                        marginTop: 4,
                        fontSize: Layout["dim"](2, "h"),
                      }}
                    >
                      {name}
                    </Text>
                  </View>
                  <View flex={0.3}></View>
                </View>
                <View flex row style={{ alignItems: "center" }}>
                  <View flex style={{ justifyContent: "center" }}>
                    <View style={{ marginTop: 4 }} row>
                      {[0, 1, 2].map((index) => {
                        return (
                          <Text
                            color="grey"
                            key={index}
                            style={{
                              fontFamily: "rubikMedium",
                              width: "25%",
                              fontSize: Layout["dim"](1.5, "h"),
                              letterSpacing: 5,
                            }}
                          >
                            ****
                          </Text>
                        );
                      })}
                      <Text
                        color="grey"
                        style={{
                          fontFamily: "rubikMedium",
                          width: "25%",
                          fontSize: Layout["dim"](1.5, "h"),
                          letterSpacing: 5,
                        }}
                      >
                        {last4}
                      </Text>
                    </View>
                  </View>
                  <View flex={0.3} middle>
                    <Image
                      source={
                        brand === "Visa"
                          ? require("../assets/images/visa.png")
                          : require("../assets/images/master.png")
                      }
                      style={{ width: 30, height: 30 }}
                    />
                  </View>
                </View>
                <View flex row style={{ alignItems: "center" }}>
                  <View flex row>
                    <View flex>
                      <View row style={{ alignItems: "center" }}>
                        <Text
                          color="grey"
                          style={{
                            fontFamily: "rubikMedium",
                            marginTop: 4,
                            fontSize: Layout["dim"](2, "h"),
                          }}
                        >
                          {expMonth}
                        </Text>
                        <Text
                          color="grey"
                          style={{
                            fontFamily: "rubikMedium",
                            marginTop: 4,
                            fontSize: Layout["dim"](2, "h"),
                            marginHorizontal: 2,
                          }}
                        >
                          /
                        </Text>
                        <Text
                          color="grey"
                          style={{
                            fontFamily: "rubikMedium",
                            marginTop: 4,
                            fontSize: Layout["dim"](2, "h"),
                          }}
                        >
                          {expYear}
                        </Text>
                      </View>
                    </View>
                    <View flex={0.3}>
                      <Text
                        color="grey"
                        style={{
                          fontFamily: "rubikMedium",
                          marginTop: 4,
                          fontSize: Layout["dim"](2, "h"),
                        }}
                      >
                        ***
                      </Text>
                    </View>
                  </View>
                  <View flex={0.3} middle>
                    <FontAwesome name="credit-card" color="grey" size={15} />
                  </View>
                </View>
              </View>
              <Touchable
                onPress={() => setAddingCard(true)}
                style={{
                  position: "absolute",
                  right: 15,
                  top: 20,
                  borderColor: "#F2EA27",
                  borderWidth: 2,
                  borderStyle: "dashed",
                  borderRadius: 30,
                  width: 40,
                  height: 40,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Ionicons name="md-add" color="#F2EA27" size={20} />
              </Touchable>
            </View>
          ) : (
            <Touchable
              onPress={() => setAddingCard(true)}
              style={{
                borderColor: "#F2EA27",
                borderWidth: 2,
                borderStyle: "dashed",
                borderRadius: 30,
                width: 60,
                height: 60,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Ionicons name="md-add" color="#F2EA27" size={30} />
            </Touchable>
          )}
        </View>
      </View>
      <View
        flex
        backgroundColor="#F2EA27"
        style={{
          borderTopRightRadius: 40,
          borderTopLeftRadius: 40,
          paddingTop: 50,
        }}
      >
        <View
          row
          height={5}
          style={{ paddingHorizontal: Layout["dim"](8, "w") }}
        >
          <Text size="small" font="rubikMedium">
            Transactions
          </Text>
        </View>
        <ScrollView
          contentContainerStyle={{ alignItems: "center", paddingTop: 20 }}
          showsVerticalScrollIndicator={false}
        >
          {donations &&
            donations.length > 0 &&
            donations.map((donation, index) => {
              const { _id, amount, createdAt, status } = donation;
              return (
                <View key={_id} row height={10} width={90} style={{}}>
                  <View flex={0.5}>
                    <Image
                      style={{ width: "100%", height: "100%" }}
                      source={require("../assets/images/blm-symbol-rough-black.png")}
                      resizeMode="contain"
                    />
                  </View>
                  <View flex style={{ justifyContent: "center" }}>
                    <Text>${amount}</Text>
                    <Text>{status}</Text>
                  </View>
                  <View flex={0.5} style={{ justifyContent: "center" }}>
                    <Text>
                      {moment(createdAt).format("MMM Do YY, h:mm:ss a'")}
                    </Text>
                  </View>
                </View>
              );
            })}
        </ScrollView>
      </View>
      <AddCard
        addingCard={addingCard}
        setAddingCard={(val) => setAddingCard(val)}
      />
    </View>
  );
};
const styles = StyleSheet.create({});
export default History;
