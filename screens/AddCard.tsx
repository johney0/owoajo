import React, { useState, useEffect, useRef } from "react";
import { StyleSheet, Platform, TextInput, Image, Alert } from "react-native";
import Modal from "react-native-modal";
import { View, Text, useThemeColor, Button } from "../components/Themed";
import { Ionicons, AntDesign, FontAwesome } from "@expo/vector-icons";
import Layout from "../constants/Layout";
import { PaymentsStripe as Stripe } from "expo-payments-stripe";
import { addCard } from "../redux/actions";
import { useDispatch } from "react-redux";
// import stripe from "tipsi-stripe";

interface AddCardProps {
  addingCard: boolean;
  setAddingCard: (value: boolean) => void;
}

const AddCard: React.FC<AddCardProps> = ({ addingCard, setAddingCard }) => {
  const [name, setName] = useState("");
  const [number, setNumber] = useState("");
  const [expMonth, setExpMonth] = useState(0);
  const [expYear, setExpYear] = useState(0);
  const [cvc, setCvc] = useState("");
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [state, setState] = useState("");
  const [country, setCountry] = useState("");
  const [zip, setZip] = useState("22304");
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const stripeAddCard = async () => {
    setLoading(true);
    const params = {
      number,
      expMonth,
      expYear,
      cvc,
      name,
      currency: "usd",
      addressLine1: address,
      addressCity: city,
      addressState: state,
      addressCountry: country,
      addressZip: zip,
    };

    await Stripe.createTokenWithCardAsync(params)
      .then((res) =>
        dispatch(
          addCard(
            res,
            (value) => (
              setLoading(false),
              value.success
                ? Alert.alert(
                    "Added Card Succesfully",
                    "Welldone",
                    [
                      {
                        text: "OK",
                        onPress: () => setAddingCard(false),
                      },
                    ],
                    { cancelable: false }
                  )
                : Alert.alert("Adding card failed", value.error)
            )
          )
        )
      )
      .catch((err) => Alert.alert("Adding card failed", err));
  };

  return (
    <Modal
      avoidKeyboard
      isVisible={addingCard}
      style={{
        justifyContent: "flex-end",
        margin: 0,
        backgroundColor: "rgba(0,0,0,.2)",
      }}
      onSwipeComplete={() => (setAddingCard(false), setLoading(false))}
      swipeDirection={["down"]}
      onBackdropPress={() => (setAddingCard(false), setLoading(false))}
      animationInTiming={800}
      animationOutTiming={800}
    >
      <View
        backgroundColor="#f5f5f5"
        width={100}
        height={45}
        style={{
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          padding: 20,
        }}
      >
        <View flex={0.5} row space="between" style={{ alignItems: "center" }}>
          <View flex>
            <Text size="small" color="black" font="rubikMedium">
              Add your card
            </Text>
            <Text size="tiny" color="grey" font="rubik">
              Fill in the fields below
            </Text>
          </View>
          <View flex={0.3} middle>
            <View
              backgroundColor="black"
              style={{ width: 50, height: 50, borderRadius: 25 }}
              middle
            >
              <Ionicons
                name="ios-lock"
                color={useThemeColor({}, "primary")}
                size={25}
              />
            </View>
          </View>
        </View>
        <View flex>
          <View flex row style={{ alignItems: "center" }}>
            <View flex style={{ justifyContent: "center" }}>
              <Text size="tiny" color="grey" font="rubikMedium">
                ENTER NAME ON CARD
              </Text>
              <TextInput
                value={name}
                onChangeText={(val) => setName(val)}
                autoFocus
                style={{
                  fontFamily: "rubikMedium",
                  marginTop: 4,
                  fontSize: Layout["dim"](2, "h"),
                }}
              />
            </View>
            <View flex={0.3}></View>
          </View>
          <View flex row style={{ alignItems: "center" }}>
            <View flex style={{ justifyContent: "center" }}>
              <Text size="tiny" color="grey" font="rubikMedium">
                ENTER CARD NUMBER
              </Text>
              <View style={{ marginTop: 4 }} row>
                {/* {[0, 1, 2, 3].map((index) => {
                  const [digits, setDigits] = useState("");
                  useEffect(() => {
                    digits.length === 4 && setNumber(number + digits);
                  }, [digits]);
                  return (
                    <TextInput
                      // ref={"box" + index}
                      value={digits}
                      onChangeText={(val) => setDigits(val)}
                      maxLength={4}
                      style={{
                        fontFamily: "rubikMedium",
                        width: "25%",
                        fontSize: Layout["dim"](2, "h"),
                        letterSpacing: 5,
                      }}
                    />
                  );
                })} */}
                <TextInput
                  // ref={"box" + index}
                  value={number}
                  onChangeText={(val) => setNumber(val)}
                  style={{
                    fontFamily: "rubikMedium",
                    // width: "25%",
                    fontSize: Layout["dim"](2, "h"),
                    letterSpacing: 5,
                    flex: 1,
                  }}
                />
              </View>
            </View>
            <View flex={0.3} middle>
              <Image
                source={require("../assets/images/master.png")}
                style={{ width: 30, height: 30 }}
              />
            </View>
          </View>
          <View flex row style={{ alignItems: "center" }}>
            <View flex row>
              <View flex>
                <Text size="tiny" color="grey" font="rubikMedium">
                  EXPIRY / VALIDITY DATE
                </Text>
                <View row style={{ alignItems: "center" }}>
                  <TextInput
                    value={expMonth ? expMonth.toString() : "0"}
                    onChangeText={(val) => setExpMonth(parseInt(val))}
                    maxLength={2}
                    style={{
                      fontFamily: "rubikMedium",
                      marginTop: 4,
                      fontSize: Layout["dim"](2, "h"),
                    }}
                  />
                  <Text
                    style={{
                      fontFamily: "rubikMedium",
                      marginTop: 4,
                      fontSize: Layout["dim"](2, "h"),
                      marginHorizontal: 2,
                    }}
                  >
                    /
                  </Text>
                  <TextInput
                    value={expYear.toString()}
                    onChangeText={(val) => setExpYear(parseInt(val))}
                    maxLength={2}
                    style={{
                      fontFamily: "rubikMedium",
                      marginTop: 4,
                      fontSize: Layout["dim"](2, "h"),
                    }}
                  />
                </View>
              </View>
              <View flex={0.3}>
                <Text size="tiny" color="grey" font="rubikMedium">
                  CVV
                </Text>
                <TextInput
                  value={cvc}
                  onChangeText={(val) => setCvc(val)}
                  maxLength={3}
                  style={{
                    fontFamily: "rubikMedium",
                    marginTop: 4,
                    fontSize: Layout["dim"](2, "h"),
                  }}
                />
              </View>
            </View>
            <View flex={0.3} middle>
              <FontAwesome name="credit-card" color="grey" size={15} />
            </View>
          </View>
        </View>
        <View flex={0.5} middle>
          <Button
            loading={loading}
            shadowless
            style={{ width: "80%" }}
            color="black"
            onPress={() => stripeAddCard()}
          >
            Add Card
          </Button>
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({});
export default AddCard;
