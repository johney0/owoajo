import React from "react";
import { StyleSheet, ImageBackground, Dimensions } from "react-native";
import { View, Text, Button } from "../components/Themed";

const { width, height } = Dimensions.get("window");

interface QuoteScreenProps {
  navigation: any;
}

const QuoteScreen: React.FC<QuoteScreenProps> = ({ navigation }) => {
  return (
    <View flex center space="around">
      <ImageBackground
        source={require("../assets/images/quote.png")}
        style={{ width, height }}
      >
        <View flex backgroundColor="rgba(242,234,39,.75)">
          <View flex center space="around">
            <View middle>
              <Text font="rubikBold">“For Your Soul”</Text>
              <Text font="rubik">(Quote of the Day)</Text>
            </View>
            <Button
              color="#000"
              round
              shadowless
              onPress={() => navigation.navigate("QuestionScreen")}
            >
              <Text color="#fff">I'm Feeling This!</Text>
            </Button>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};
const styles = StyleSheet.create({});
export default QuoteScreen;
