import React from "react";
import { StyleSheet, ImageBackground } from "react-native";
import Touchable from "react-native-platform-touchable";
import { Switch } from "react-native-paper";
import Layout from "../constants/Layout";
import { Ionicons, AntDesign } from "@expo/vector-icons";
import { Text, View } from "../components/Themed";

interface SettingsProps {
  navigation: any;
}

const Settings: React.FC<SettingsProps> = ({ navigation }) => {
  return (
    <View style={styles.Container}>
      <ImageBackground
        source={require("../assets/images/settings.jpg")}
        style={{
          width: "100%",
          height: "100%",
        }}
        imageStyle={{ borderTopLeftRadius: 20, borderTopRightRadius: 20 }}
      >
        <View flex backgroundColor="rgba(0,0,0,.75)" middle>
          <View
            flex={0.5}
            row
            style={{
              alignSelf: "stretch",
              justifyContent: "flex-start",
              alignItems: "center",
              paddingLeft: Layout["dim"](5, "w"),
              backgroundColor: "primary",
              paddingTop: Layout["dim"](5, "h"),
            }}
          >
            <Touchable
              style={{
                backgroundColor: "transparent",

                justifyContent: "center",
                alignItems: "flex-start",
                flex: 0.2,
              }}
              onPress={() => navigation.goBack()}
            >
              <Ionicons color="#f5f5f5" size={24} name="ios-arrow-round-back" />
            </Touchable>
            <View flex>
              <Text
                size={Layout["dim"](2.46, "h")}
                style={{ fontFamily: "rubikBold" }}
                color="#f5f5f5"
              >
                Notifications
              </Text>
            </View>
          </View>

          <View
            flex={4.5}
            style={{
              alignSelf: "stretch",
              paddingRight: Layout["dim"](7.733, "w"),
              paddingLeft: Layout["dim"](5, "w"),
            }}
          >
            <View flex={5} style={{ marginTop: Layout["dim"](5, "h") }}>
              <Touchable
                style={{
                  height: Layout["dim"](12, "h"),
                  marginBottom: Layout["dim"](2, "h"),
                }}
              >
                <View
                  flex
                  style={{
                    borderBottomWidth: 0.25,
                    borderBottomColor: "#ddd",
                    paddingBottom: Layout["dim"](2, "h"),
                  }}
                  row
                  space="around"
                >
                  <View
                    flex
                    style={{
                      justifyContent: "space-around",
                    }}
                  >
                    <Text
                      size={"medium"}
                      color="primary"
                      style={{
                        fontWeight: "300",
                        fontFamily: "rubikMedium",
                        paddingTop: Layout["dim"](0.5, "h"),
                      }}
                    >
                      Device Notifications
                    </Text>
                    <Text
                      size="small"
                      color="#f5f5f5"
                      style={{
                        fontFamily: "rubik",
                        paddingVertical: Layout["dim"](1, "h"),
                      }}
                    >
                      Opt in to receive notifications on this device
                    </Text>
                  </View>
                  <View
                    flex={0.2}
                    style={{
                      alignItems: "center",
                      justifyContent: "flex-start",
                    }}
                  >
                    <Switch
                      // value={phoneToggle}
                      // onValueChange={(val) => phoneNotifToggler(val)}
                      color={"primary"}
                    />
                  </View>
                </View>
              </Touchable>
              <Touchable
                style={{
                  height: Layout["dim"](12, "h"),
                  marginBottom: Layout["dim"](2, "h"),
                }}
              >
                <View
                  flex
                  style={{
                    borderBottomWidth: 0.25,
                    borderBottomColor: "#ddd",
                    paddingBottom: Layout["dim"](2, "h"),
                  }}
                  row
                  space="around"
                >
                  <View
                    flex
                    style={{
                      justifyContent: "space-around",
                    }}
                  >
                    <Text
                      size="medium"
                      color={"primary"}
                      style={{
                        fontWeight: "300",
                        fontFamily: "rubikMedium",
                        paddingTop: Layout["dim"](0.5, "h"),
                      }}
                    >
                      Share Options
                    </Text>
                    <Text
                      size="small"
                      color="#f5f5f5"
                      style={{
                        fontFamily: "rubik",
                        paddingVertical: Layout["dim"](1, "h"),
                      }}
                    >
                      Opt in to share your donations
                    </Text>
                  </View>
                  <View
                    flex={0.2}
                    style={{
                      alignItems: "center",
                      justifyContent: "flex-start",
                    }}
                  >
                    <Switch
                      // value={phoneToggle}
                      // onValueChange={(val) => phoneNotifToggler(val)}
                      color={"primary"}
                    />
                  </View>
                </View>
              </Touchable>
              <Touchable
                style={{
                  height: Layout["dim"](8, "h"),
                  marginBottom: Layout["dim"](2, "h"),
                }}
              >
                <View
                  flex
                  style={{
                    borderBottomWidth: 0.25,
                    borderBottomColor: "#ddd",
                    paddingBottom: Layout["dim"](2, "h"),
                  }}
                  row
                  space="around"
                >
                  <View
                    flex
                    style={{
                      justifyContent: "space-around",
                    }}
                  >
                    <Text
                      size={"medium"}
                      color={"primary"}
                      style={{
                        fontWeight: "300",
                        fontFamily: "rubikMedium",
                        paddingTop: Layout["dim"](0.5, "h"),
                      }}
                    >
                      Feedback
                    </Text>
                    <Text
                      size="small"
                      color="#f5f5f5"
                      style={{
                        fontFamily: "rubik",
                        paddingVertical: Layout["dim"](1, "h"),
                      }}
                    >
                      Rate and Review
                    </Text>
                  </View>
                  <View flex={0.2} middle>
                    <Ionicons
                      name="ios-arrow-forward"
                      size={24}
                      color="#f5f5f5"
                    />
                  </View>
                </View>
              </Touchable>
              <Touchable
                style={{
                  height: Layout["dim"](6, "h"),
                  marginBottom: Layout["dim"](2, "h"),
                }}
              >
                <View
                  flex
                  style={{
                    borderBottomWidth: 0.25,
                    borderBottomColor: "#ddd",
                    paddingBottom: Layout["dim"](2, "h"),
                  }}
                  row
                  space="around"
                >
                  <View
                    flex
                    style={{
                      justifyContent: "space-around",
                    }}
                  >
                    <Text
                      size="medium"
                      color="primary"
                      style={{
                        fontWeight: "300",
                        fontFamily: "rubikMedium",
                        paddingTop: Layout["dim"](0.5, "h"),
                      }}
                    >
                      Terms of use
                    </Text>
                  </View>
                  <View flex={0.2} middle>
                    <Ionicons
                      name="ios-arrow-forward"
                      size={24}
                      color="#f5f5f5"
                    />
                  </View>
                </View>
              </Touchable>
              <Touchable
                style={{
                  height: Layout["dim"](6, "h"),
                  marginBottom: Layout["dim"](2, "h"),
                }}
              >
                <View
                  flex
                  style={{
                    borderBottomWidth: 0.25,
                    borderBottomColor: "#ddd",
                    paddingBottom: Layout["dim"](2, "h"),
                  }}
                  row
                  space="around"
                >
                  <View
                    flex
                    style={{
                      justifyContent: "space-around",
                    }}
                  >
                    <Text
                      size="medium"
                      color="primary"
                      style={{
                        fontWeight: "300",
                        fontFamily: "rubikMedium",
                        paddingTop: Layout["dim"](0.5, "h"),
                      }}
                    >
                      Privacy Policy
                    </Text>
                  </View>
                  <View flex={0.2} middle>
                    <Ionicons
                      name="ios-arrow-forward"
                      size={24}
                      color="#f5f5f5"
                    />
                  </View>
                </View>
              </Touchable>
            </View>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f5f5f5",
  },
});
export default Settings;
