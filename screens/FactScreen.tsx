import React from "react";
import { StyleSheet, Dimensions, ImageBackground } from "react-native";
import { useDispatch } from "react-redux";
import { View, Text, Button } from "../components/Themed";
const { width, height } = Dimensions.get("window");

interface FactScreenProps {
  navigation: any;
}

const FactScreen: React.FC<FactScreenProps> = ({ navigation }) => {
  const dispatch = useDispatch();

  return (
    <View flex center space="around">
      <ImageBackground
        source={require("../assets/images/fact.jpg")}
        style={{ width, height }}
      >
        <View flex backgroundColor="rgba(0,0,0,.75)">
          <View flex center space="around">
            <View middle>
              <Text color="#fff" font="rubikBold">
                “For Your Mind
              </Text>
              <Text color="#fff" font="rubikMedium">
                (Fact of the Day)
              </Text>
            </View>
            <View row>
              <Button
                color="#000"
                round
                shadowless
                onPress={() => navigation.navigate("LaterScreen")}
              >
                <Text color="#fff">I'm Out!</Text>
              </Button>
              <Button
                color="#000"
                round
                shadowless
                onPress={() =>
                  dispatch({
                    type: "SET_QUOTE_READ",
                    payload: true,
                  })
                }
              >
                <Text color="#fff">Handle Some Bussiness</Text>
              </Button>
            </View>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};
const styles = StyleSheet.create({});
export default FactScreen;
