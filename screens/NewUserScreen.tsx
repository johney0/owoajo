import React, { useRef, useState } from "react";
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  ScrollView,
} from "react-native";
import { View, Text, Button } from "../components/Themed";
import Layout from "../constants/Layout";
import Modal from "react-native-modal";
import { useDispatch } from "react-redux";

const { width, height } = Dimensions.get("window");

interface NewUserScreenProps {
  navigation: any;
}

const NewUserScreen: React.FC<NewUserScreenProps> = ({ navigation }) => {
  const scroll = useRef(null);
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  return (
    <View flex center space="around">
      <ScrollView
        horizontal
        snapToInterval={width}
        scrollEventThrottle={10}
        ref={scroll}
        scrollEnabled={false}
        showsHorizontalScrollIndicator={false}
        nestedScrollEnabled
      >
        <ImageBackground
          source={require("../assets/images/quote.png")}
          style={{ width, height }}
        >
          <View flex backgroundColor="rgba(242,234,39,.75)">
            <View
              flex
              center
              space="around"
              style={{ paddingHorizontal: Layout["dim"](8, "w") }}
            >
              <Text font="rubikMedium" center color="black" size="small">
                Because you are over 18, you can now help grow our Black Pennies
                Matter community
              </Text>
              <Button
                color="#000"
                round
                shadowless
                style={{ position: "absolute", bottom: Layout["dim"](5, "h") }}
                onPress={() => setOpen(true)}
              >
                <Text color="#fff">Tell me more</Text>
              </Button>
            </View>
          </View>
        </ImageBackground>
        <ImageBackground
          source={require("../assets/images/settings.jpg")}
          style={{ width, height }}
        >
          <View flex backgroundColor="rgba(242,234,39,.75)">
            <View
              flex
              center
              space="around"
              style={{ paddingHorizontal: Layout["dim"](8, "w") }}
            >
              <Text font="rubikMedium" center color="black" size="small">
                You may see Black Pennies Matter aggregate several days into one
                transaction (usually 50 cents or 1 dollar) but never more than
                $3.65 in a 12 month period ($3.66 in leap years).
              </Text>
              <Button
                color="#000"
                round
                shadowless
                style={{ position: "absolute", bottom: Layout["dim"](5, "h") }}
                onPress={() =>
                  dispatch({
                    type: "SET_NEW_USER",
                    payload: false,
                  })
                }
              >
                <Text color="#fff">Cool</Text>
              </Button>
            </View>
          </View>
        </ImageBackground>
      </ScrollView>
      <Modal
        propagateSwipe
        avoidKeyboard
        isVisible={open}
        style={{
          justifyContent: "center",
          margin: 0,
          backgroundColor: "transparent",
        }}
        onSwipeComplete={() => setOpen(false)}
        swipeDirection={["down"]}
        onBackdropPress={() => setOpen(false)}
        animationInTiming={800}
        animationOutTiming={800}
      >
        <View
          height={25}
          backgroundColor="#f5f5f5"
          center
          space="around"
          style={{ paddingHorizontal: Layout["dim"](8, "w"), width }}
        >
          <Text font="rubikMedium" center color="black" size="tiny">
            By continuing, you are agreeing to donate one cent to Black Pennies
            Matter each day you say yes to the question “Do Black Pennies Matter
            today?”
          </Text>
          <Button
            color="#000"
            round
            shadowless
            onPress={() => (
              setOpen(false), scroll.current.scrollTo({ x: width })
            )}
          >
            <Text color="#fff">So Far, So Good</Text>
          </Button>
        </View>
      </Modal>
    </View>
  );
};
const styles = StyleSheet.create({});
export default NewUserScreen;
