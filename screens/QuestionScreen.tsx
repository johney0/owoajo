import React, { useState } from "react";
import { StyleSheet, ImageBackground, Dimensions, Image } from "react-native";
import { View, Text, Button } from "../components/Themed";
import Modal from "react-native-modal";
const { width, height } = Dimensions.get("window");

interface QuestionScreenProps {
  navigation: any;
}

const QuestionScreen: React.FC<QuestionScreenProps> = ({ navigation }) => {
  const [showModal, setShowModal] = useState(false);
  return (
    <View flex center space="around">
      <ImageBackground
        source={require("../assets/images/question.jpg")}
        style={{ width, height }}
      >
        <View flex backgroundColor="rgba(242,234,39,.75)">
          <View flex center space="around">
            <View middle>
              <Text size="big" font="rubikBold">
                Do
              </Text>
              <Image
                source={require("../assets/images/bpm.png")}
                style={{ resizeMode: "contain", width: 200, height: 100 }}
              />
              <Text size="big" font="rubikBold">
                today?
              </Text>
            </View>
            <View row>
              <Button
                color="#000"
                shadowless
                onPress={() => navigation.navigate("FactScreen")}
              >
                <Text color="#fff" size="tiny" font="rubikMedium">
                  Damn right they do!
                </Text>
              </Button>
              <Button
                color="#000"
                shadowless
                onPress={() => setShowModal(true)}
              >
                <Text color="#fff" center size="tiny" font="rubikMedium">
                  naw they dont change a thing
                </Text>
              </Button>
            </View>
          </View>
        </View>
      </ImageBackground>

      <Modal
        avoidKeyboard
        isVisible={showModal}
        style={{
          justifyContent: "flex-end",
          margin: 0,
        }}
        onSwipeComplete={() => setShowModal(false)}
        swipeDirection={["down"]}
        onBackdropPress={() => setShowModal(false)}
        animationInTiming={800}
        animationOutTiming={800}
      >
        <View
          backgroundColor="#f5f5f5"
          width={100}
          height={45}
          space="around"
          style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20 }}
        >
          <ImageBackground
            source={require("../assets/images/modal.jpg")}
            style={{
              width: "100%",
              height: "100%",
            }}
            imageStyle={{ borderTopLeftRadius: 20, borderTopRightRadius: 20 }}
          >
            <View
              flex
              backgroundColor="rgba(0,0,0,.75)"
              style={{
                padding: 20,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}
            >
              <View flex center space="around">
                <View middle>
                  <Text color="#f5f5f5">Just making sure</Text>
                  <Text color="#f5f5f5">Black Pennis Matter</Text>
                  <Text color="#f5f5f5">Dont they?</Text>
                </View>
                <View row>
                  <Button
                    color="#fff"
                    shadowless
                    onPress={() => (
                      setShowModal(false), navigation.navigate("FactScreen")
                    )}
                  >
                    <Text color="#000">Yes they absolutely do!</Text>
                  </Button>
                  <Button
                    color="#fff"
                    shadowless
                    onPress={() => (
                      setShowModal(false), navigation.navigate("FactScreen")
                    )}
                  >
                    <Text color="#000" center>
                      No, one penny changes nothing
                    </Text>
                  </Button>
                </View>
              </View>
            </View>
          </ImageBackground>
        </View>
      </Modal>
    </View>
  );
};
const styles = StyleSheet.create({});
export default QuestionScreen;
