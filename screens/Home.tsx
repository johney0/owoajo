import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  Image,
  Share,
  Alert,
  SafeAreaView,
  ScrollView,
  RefreshControl,
  Dimensions,
  Platform,
  StatusBar,
  ImageBackground,
} from "react-native";
import { View, Text, useThemeColor } from "../components/Themed";
import Touchable from "react-native-platform-touchable";
import Layout from "../constants/Layout";
import { Ionicons, AntDesign } from "@expo/vector-icons";
import * as Animatable from "react-native-animatable";
import { useDispatch, useSelector } from "react-redux";
import { getProfile } from "../redux/actions/settings";
import { donate, getDonations } from "../redux/actions/payments";
import { ActivityIndicator } from "react-native-paper";
import { wait } from "../components/Functions";
import { logout } from "../redux/actions/authentication";
import jwt_decode from "jwt-decode";
import AsyncStorage from "@react-native-community/async-storage";
import { useFocusEffect } from "@react-navigation/native";
import NotifService from "../components/NotifService.js";
import moment from "moment";
const { width, height } = Dimensions.get("screen");
interface HomeProps {
  navigation: any;
}

const Home: React.FC<HomeProps> = ({ navigation }) => {
  const [quote, setQuote] = useState(
    "One had better die fighting against injustice than to die like a dog or a rat in a trap"
  );
  const [donating, setDonating] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(() => {
    dispatch(getProfile());

    dispatch(getDonations());
    wait(2000).then(() => setRefreshing(false));
  }, [refreshing]);
  const fetch = async () => {
    const token = await AsyncStorage.getItem("user");
    token && jwt_decode(token).exp >= Date.now() / 1000
      ? console.log("valid user")
      : dispatch(logout());
  };

  useFocusEffect(
    React.useCallback(() => {
      StatusBar.setBarStyle("dark-content");
      Platform.OS === "android" && StatusBar.setBackgroundColor("#F2EA27");
    }, [])
  );
  const onShare = async () => {
    try {
      const result = await Share.share({
        message: quote,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  const dispatch = useDispatch();
  useEffect(() => {
    fetch();
    dispatch(getProfile());
    dispatch(getDonations());
  }, []);
  const { Profile }: any = useSelector(
    (state: { settings: object }) => state.settings
  );
  const { donationAmount } = Profile;
  //Notifications
  // const [registerToken, setRegisterToken] = useState();
  // const [fcm, setFcm] = useState(false);
  // useEffect(() => {
  //   notif.getScheduledLocalNotifications(
  //     (notifs) =>
  //       notifs.length < 1 &&
  //       Alert.alert(
  //         "Welcome",
  //         "Automate your donations?",
  //         [
  //           {
  //             text: "OK",
  //             onPress: () => notif.scheduleRepeatNotif(),
  //           },
  //           {
  //             text: "Later",
  //             onPress: () => null,
  //           },
  //         ],
  //         { cancelable: false }
  //       )
  //   );
  // }, []);
  // const onRegister = (token) => {
  //   setFcm(true);
  //   setRegisterToken(token.token);
  // };

  // const onNotif = (notif) => {
  //   Alert.alert(notif.title, notif.message);
  // };

  // const handlePerm = (perms) => {
  //   Alert.alert("Permissions", JSON.stringify(perms));
  // };
  // const notif = new NotifService(onRegister, onNotif);

  return (
    // <SafeAreaView style={styles.container}>
    <ScrollView
      showsVerticalScrollIndicator={false}
      contentContainerStyle={styles.scrollView}
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={() => onRefresh()}
          tintColor={"#14171a"}
          colors={["#14171a"]}
        />
      }
    >
      <ImageBackground
        source={require("../assets/images/fact.jpg")}
        style={{ width, height }}
      >
        <View
          backgroundColor="rgba(242,234,39,.75)"
          flex
          // style={{ paddingVertical: Layout["dim"](1, "h") }}
        >
          <View
            height={20}
            row
            style={{
              alignItems: "center",
              paddingHorizontal: Layout["dim"](8, "w"),
            }}
            space="between"
          >
            <Touchable onPress={() => navigation.toggleDrawer()}>
              <AntDesign
                name="appstore1"
                color="#14171a"
                size={Layout["dim"](4, "h")}
              />
            </Touchable>
            <Image
              style={{
                width: Layout["dim"](8, "w"),
                height: Layout["dim"](12, "w"),
              }}
              source={require("../assets/images/fist.png")}
            />
          </View>
          <View
            flex={0.4}
            center
            style={{
              paddingHorizontal: Layout["dim"](8, "w"),
            }}
          >
            <Text
              font="rubikItalic"
              size="small"
              color="#14171a"
              center
              style={{ fontStyle: "italic" }}
            >
              <Text size="huge" color="#14171a">
                "
              </Text>
              {quote}

              <Text
                size="huge"
                color="#14171a"
                style={{ textAlign: "right", fontStyle: "italic" }}
              >
                "
              </Text>
            </Text>

            <View row middle style={{ paddingTop: 10 }}>
              <View
                backgroundColor="#14171a"
                style={{ height: 3, width: 20, marginRight: 10 }}
              />
              <Text size="small" color="#14171a">
                Ida B.Wells
              </Text>
            </View>
            <Touchable onPress={() => console.log("notif")}>
              <View
                middle
                style={{
                  width: 30,
                  height: 30,
                  borderRadius: 15,
                  marginTop: 10,
                }}
                backgroundColor="#14171a"
              >
                <Ionicons name="ios-share-alt" color="#F2EA27" size={15} />
              </View>
            </Touchable>
          </View>
          {/* <View flex middle>
          <Touchable
            style={{
              transform: [{ rotate: "45deg" }],
              borderRadius: Layout["dim"](21.25, "w"),
            }}
            onPress={() => (
              setDonating(true),
              dispatch(
                donate(
                  (result) => (
                    setDonating(false),
                    result.success
                      ? Alert.alert("Donated Succesfully", "Thank you")
                      : Alert.alert("Donation failed", result.error)
                  )
                )
              )
            )}
          >
            <Animatable.View
              animation="pulse"
              easing="ease-out"
              iterationCount="infinite"
              style={{
                backgroundColor: "#14171a",
                width: Layout["dim"](42.5, "w"),
                height: Layout["dim"](42.5, "w"),
                borderRadius: Layout["dim"](21.25, "w"),
                borderTopColor: "#fff",
                borderWidth: 5,
                borderColor: "#14171a",
                shadowColor: "grey",
                shadowOffset: { width: 1, height: 4 },
                shadowOpacity: 0.5,
                shadowRadius: 5,
                elevation: 0.3,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              {donating ? (
                <ActivityIndicator color="#F2EA27" />
              ) : (
                <View middle flex style={{ transform: [{ rotate: "-45deg" }] }}>
                  <Text font="rubikMedium" color="#fff">
                    Tap
                  </Text>
                  <Text font="rubikMedium" color="#fff">
                    To Donate
                  </Text>
                  <Text size="tiny" font="rubikMedium" color="#F2EA27">
                    ${donationAmount}
                  </Text>
                </View>
              )}
            </Animatable.View>
          </Touchable>
        </View> */}
          {/* 
          <View
            row
            flex={0.8}
            // backgroundColor="red"
            // style={{ paddingHorizontal: Layout["dim"](4, "w") }}
          >
            <View flex middle>
              <Image
                style={{
                  width: "100%",
                  height: "100%",
                }}
                resizeMode="contain"
                source={require("../assets/images/blm-george-floyd.png")}
              />
            </View>
            <View flex middle>
              <Image
                style={{
                  width: "100%",
                  height: "100%",
                }}
                resizeMode="contain"
                source={require("../assets/images/blm-poster-justice.png")}
              />
            </View>
          </View> */}
        </View>
      </ImageBackground>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    minHeight: Math.round(Dimensions.get("screen").height),
    backgroundColor: "#F2EA27",
  },
  scrollView: {
    flex: 1,
    width,
    backgroundColor: "#F2EA27",
  },
});
export default Home;
