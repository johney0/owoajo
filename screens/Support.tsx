import React from "react";
import { StyleSheet, View } from "react-native";
import { Block, Text } from "galio-framework";

interface SupportProps {}

const Support: React.FC<SupportProps> = ({}) => {
  return (
    <Block flex middle>
      <Text>Chat with us</Text>
    </Block>
  );
};
const styles = StyleSheet.create({});
export default Support;
