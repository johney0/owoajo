import React from "react";
import { StyleSheet, ImageBackground, Dimensions } from "react-native";
import { View, Text, Button } from "../components/Themed";

const { width, height } = Dimensions.get("window");

interface LaterScreenProps {
  navigation: any;
}

const LaterScreen: React.FC<LaterScreenProps> = ({ navigation }) => {
  return (
    <View flex center space="around">
      <ImageBackground
        source={require("../assets/images/settings.jpg")}
        style={{ width, height }}
      >
        <View flex backgroundColor="rgba(242,234,39,.75)">
          <View flex center space="around">
            <View middle>
              <Text font="rubikBold" size="small">
                Come back tomorrow
              </Text>
            </View>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};
const styles = StyleSheet.create({});
export default LaterScreen;
