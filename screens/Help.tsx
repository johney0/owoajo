import { AntDesign } from "@expo/vector-icons";
import React from "react";
import { StyleSheet, ImageBackground, Dimensions, Image } from "react-native";
import Touchable from "react-native-platform-touchable";
import { View, Text, Button } from "../components/Themed";
import Layout from "../constants/Layout";
interface HelpProps {
  navigation: any;
}

const Help: React.FC<HelpProps> = ({ navigation }) => {
  return (
    <View flex middle>
      <ImageBackground
        source={require("../assets/images/construction.jpg")}
        style={{
          width: "100%",
          height: "100%",
        }}
        imageStyle={{ borderTopLeftRadius: 20, borderTopRightRadius: 20 }}
      >
        <View flex backgroundColor="rgba(0,0,0,.75)" middle>
          <View
            height={20}
            row
            style={{
              alignItems: "center",
              paddingHorizontal: Layout["dim"](8, "w"),
            }}
            space="between"
          >
            <Touchable onPress={() => navigation.toggleDrawer()}>
              <AntDesign
                name="appstore1"
                color="#f5f5f5"
                size={Layout["dim"](4, "h")}
              />
            </Touchable>
          </View>

          <Text color="white" size="small" font="rubikBold">
            Under Construction
          </Text>
        </View>
      </ImageBackground>
    </View>
  );
};
const styles = StyleSheet.create({});
export default Help;
