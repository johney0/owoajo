import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  Image,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  TextInput,
} from "react-native";
import { View, Text, Button, useThemeColor } from "../../components/Themed";
import FloatingLabelInput from "react-native-floating-label-input";
import Layout from "../../constants/Layout";
import Snack from "../../components/SnackBar";
import { Ionicons, Entypo, FontAwesome5 } from "@expo/vector-icons";
import { register, resetState } from "../../redux/actions";
import { connect, useSelector, useDispatch } from "react-redux";
import { wait, isEmptyObj } from "../../components/Functions";
import RNPickerSelect from "react-native-picker-select";
import { PhoneNumberUtil } from "google-libphonenumber";
import EmailModal from "../../components/EmailModal";
const { width, height } = Dimensions.get("screen");

interface SignUpProps {
  navigation: any;
  register: (val: object) => void;
  resetState: () => void;
}

const SignUp: React.FC<SignUpProps> = ({
  navigation,
  register,
  resetState,
}) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [fullName, setFullName] = useState("");
  const [phoneCode, setPhoneCode] = useState("1");
  const [phone, setPhone] = useState("");
  const [err, setErr] = useState();
  const [loading, setLoading] = useState(false);
  const [validPhone, setValidPhone] = useState(false);
  const [emailModal, setEmailModal] = useState(false);
  const [country, setCountry] = useState("US");
  const geoCode = "+" + phoneCode;
  const phoneNumber = phoneCode ? geoCode + phone : phone;
  let newPhone = phoneCode + phone;
  phone.startsWith("0") && (newPhone = phoneCode + phone.slice(1));
  useEffect(() => {
    try {
      const phoneUtil = PhoneNumberUtil.getInstance();
      const number = phoneUtil.parse(phoneNumber, country);
      let resp = phoneUtil.isValidNumber(number);

      if (!resp) {
        setValidPhone(false);
        console.log(resp);
      } else {
        setValidPhone(true);
        console.log(resp);
      }
    } catch (e) {
      setValidPhone(false);
    }
  }, [phone]);
  useEffect(() => {
    resetState();
  }, []);
  const { errors, generic }: any = useSelector(
    (state: {
      errors: {
        SignUp?: object;
      };
      generic: {
        Success: {
          SignUp?: string;
        };
      };
    }) => state
  );
  const dispatch = useDispatch();
  useEffect(() => {
    errors && setErr(errors.SignUp);

    !isEmptyObj(errors) &&
      wait(2500).then(
        () => (
          setLoading(false),
          dispatch({
            type: "RESET_ERRORS",
            payload: {},
          })
        )
      );
  }, [errors]);

  useEffect(() => {
    generic.Success.SignUp &&
      generic.Success.SignUp.message.length > 0 &&
      (setLoading(false),
      setEmailModal(true),
      wait(1000).then(() => resetState()));
  }, [generic.Success]);

  return (
    <View flex>
      <Snack
        color="#F84949"
        visible={err && !isEmptyObj(err)}
        text={(err && err.message) || ""}
        onDismiss={() => null}
        containerStyle={{}}
      />
      <ImageBackground
        source={require("../../assets/images/blmBackround.jpg")}
        style={{ width, height }}
      >
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <KeyboardAvoidingView
            style={{ flex: 1 }}
            behavior="padding"
            keyboardVerticalOffset={-100}
          >
            <View flex>
              <View
                style={{ paddingHorizontal: Layout["dim"](8, "w") }}
                height={20}
                space="between"
                row
              >
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={{
                      width: 60,
                      height: 60,
                    }}
                    resizeMode="contain"
                    source={require("../../assets/images/blm-logo.png")}
                  />
                </View>
                <View
                  flex
                  style={{ justifyContent: "center", alignItems: "flex-end" }}
                >
                  <Image
                    style={{
                      width: 60,
                      height: 60,
                    }}
                    resizeMode="contain"
                    source={require("../../assets/images/fist.png")}
                  />
                </View>
              </View>
              <View
                flex
                backgroundColor="rgba(0,0,0,.75)"
                style={{
                  paddingHorizontal: Layout["dim"](8, "w"),
                  borderTopRightRadius: 50,
                  borderTopLeftRadius: 50,
                }}
              >
                <View flex={0.2} style={{ justifyContent: "center" }}>
                  <Text center font="rubikMedium" size="big" color="#fff">
                    BLACK PENNIES MATTER
                  </Text>
                  <Text color="#fff" font="rubikMedium" center>
                    Welcome to Your Community
                  </Text>
                </View>
                <View flex={0.5} style={{ justifyContent: "space-around" }}>
                  <FloatingLabelInput
                    label="Full Name"
                    value={fullName}
                    onChangeText={(value) => setFullName(value)}
                    containerStyles={{
                      borderWidth: 0,
                      borderBottomWidth: 1,
                      borderBottomColor: "rgba(255,255,255,.9)",
                    }}
                    labelStyles={{ color: "#fff" }}
                    inputStyles={{ color: "#fff" }}
                  />
                  <FloatingLabelInput
                    label="Email"
                    value={email}
                    onChangeText={(value) => setEmail(value)}
                    containerStyles={{
                      borderWidth: 0,
                      borderBottomWidth: 1,
                      borderBottomColor: "rgba(255,255,255,.9)",
                    }}
                    labelStyles={{ color: "#fff" }}
                    inputStyles={{ color: "#fff" }}
                  />
                  <View
                    row
                    space="between"
                    style={{
                      alignItems: "center",
                      borderBottomWidth: 1,
                      borderBottomColor: "rgba(255,255,255,.9)",
                      marginHorizontal: Layout["dim"](4, "w"),
                    }}
                  >
                    <RNPickerSelect
                      style={pickerSelectStyles}
                      useNativeAndroidPickerStyle={false}
                      placeholder={{
                        label: "+1",
                        value: "1",
                        color: "#000",
                      }}
                      onValueChange={(value) => (
                        setPhoneCode(value.code), setCountry(value.country)
                      )}
                      items={[
                        {
                          label: "+1",
                          value: { code: "1", country: "US" },
                        },
                        {
                          label: "+234",
                          value: { code: "234", country: "NG" },
                        },
                        {
                          label: "+233",
                          value: { code: "233", country: "GH" },
                        },
                      ]}
                    />
                    <TextInput
                      placeholder="Phone Number"
                      placeholderTextColor="#fff"
                      value={phone}
                      onChangeText={(value) => setPhone(value)}
                      keyboardType="phone-pad"
                      style={{
                        flex: 1,
                        backgroundColor: "transparent",
                        borderBottomWidth: 0,
                        borderBottomColor: "rgba(255,255,255,.9)",
                        color: "#fff",
                      }}
                    />
                  </View>
                  <FloatingLabelInput
                    label="Password"
                    value={password}
                    isPassword={true}
                    onChangeText={(value) => setPassword(value)}
                    containerStyles={{
                      borderWidth: 0,
                      borderBottomWidth: 1,
                      borderBottomColor: "rgba(255,255,255,.9)",
                    }}
                    labelStyles={{ color: "#fff" }}
                    inputStyles={{ color: "#fff" }}
                    showPasswordImageStyles={{ tintColor: "#fff" }}
                  />
                </View>
                <View flex={0.3} style={{ justifyContent: "center" }}>
                  <View row>
                    <View flex middle>
                      <Button
                        loading={loading}
                        color="#fff"
                        round
                        shadowless
                        onPress={() =>
                          validPhone &&
                          (setLoading(true),
                          register({
                            password,
                            phone: newPhone,
                            email,
                            fullName,
                          }))
                        }
                      >
                        <Text color="#000">Sign Up</Text>
                      </Button>
                    </View>
                    <View flex row middle>
                      <View
                        middle
                        style={{
                          width: 44,
                          height: 44,
                          borderRadius: 22,
                          marginRight: 5,
                        }}
                      >
                        <Entypo
                          name="facebook-with-circle"
                          size={44}
                          color="#fff"
                        />
                      </View>
                      <View
                        middle
                        style={{
                          width: 44,
                          height: 44,
                          borderRadius: 22,
                          padding: 0,
                        }}
                        backgroundColor="#fff"
                      >
                        <FontAwesome5
                          name="google"
                          size={22}
                          color="rgba(242,234,39,.75)2"
                        />
                      </View>
                    </View>
                  </View>

                  <Text
                    center
                    onPress={() => navigation.navigate("SignIn")}
                    font="rubikMedium"
                    size="tiny"
                    color="#fff"
                  >
                    Already have an account?{" "}
                    <Text size="small" color="rgba(242,234,39,1)">
                      Sign In
                    </Text>
                  </Text>
                  <Text center color="#fff" size={9}>
                    We will never spam you or sell your data, we are family
                    here!
                  </Text>
                </View>
              </View>
            </View>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      </ImageBackground>
      <EmailModal
        email={email}
        emailModal={emailModal}
        setEmailModal={(val) => setEmailModal(val)}
      />
    </View>
  );
};

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    paddingVertical: 6,
    paddingRight: 6,
    color: "#fff",
    textAlign: "right",
  },
  inputAndroid: {
    paddingVertical: 6,
    paddingRight: 6,
    color: "#fff",
    textAlign: "right",
  },
  viewContainer: {
    justifyContent: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors,
  auth: state.auth,
  generic: state.generic,
});

export default connect(mapStateToProps, {
  register,
  resetState,
})(SignUp);
