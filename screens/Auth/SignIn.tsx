import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  Image,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import { View, Text, Button } from "../../components/Themed";
import FloatingLabelInput from "react-native-floating-label-input";
import Layout from "../../constants/Layout";
import { Ionicons, Entypo, FontAwesome5 } from "@expo/vector-icons";
import AsyncStorage from "@react-native-community/async-storage";
import { login, resetState } from "../../redux/actions";
import { connect, useSelector, useDispatch } from "react-redux";
import Snack from "../../components/SnackBar";
import { isEmptyObj, wait } from "../../components/Functions";

const { width, height } = Dimensions.get("window");

interface SignInProps {
  navigation: any;
  login: (val: object) => void;
  resetState: () => void;
}

const SignIn: React.FC<SignInProps> = ({ navigation, login, resetState }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [err, setErr] = useState();
  const [loading, setLoading] = useState(false);
  const { errors, generic }: any = useSelector(
    (state: {
      errors: {
        SignIn?: object;
      };
      generic: {
        Success: {
          SignIn?: string;
        };
      };
    }) => state
  );
  const dispatch = useDispatch();
  useEffect(() => {
    errors && setErr(errors.SignIn);
    !isEmptyObj(errors) &&
      wait(2500).then(
        () => (
          setLoading(false),
          dispatch({
            type: "RESET_ERRORS",
            payload: {},
          })
        )
      );
  }, [errors]);

  return (
    <View flex>
      <Snack
        color="#F84949"
        visible={err && !isEmptyObj(err)}
        text={(err && err.message) || ""}
        onDismiss={() => null}
        containerStyle={{}}
      />

      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior="padding"
          keyboardVerticalOffset={-50}
        >
          <ImageBackground
            source={require("../../assets/images/blmBackround.jpg")}
            style={{ width, height }}
          >
            <View
              flex
              backgroundColor="rgba(242,234,39,.75)"
              style={{ paddingHorizontal: Layout["dim"](8, "w") }}
            >
              <View flex={2} middle>
                <View flex style={{ justifyContent: "flex-end" }}>
                  <Image
                    style={{
                      width: Layout["dim"](25, "w"),
                      height: Layout["dim"](25, "w"),
                    }}
                    resizeMode="contain"
                    source={require("../../assets/images/fist.png")}
                  />
                </View>
                <View flex middle>
                  {/* <Text font="rubikMedium" size="medium">
                BLACK
              </Text> */}
                  <Text
                    font="rubikMedium"
                    size="medium"
                    color="#fff"
                    style={{
                      paddingHorizontal: Layout["dim"](8, "w"),
                      paddingVertical: 6,
                      backgroundColor: "black",
                      marginVertical: Layout["dim"](2, "h"),
                    }}
                  >
                    BLACK PENNIES MATTER
                  </Text>
                  {/* <Text font="rubikMedium" size="medium">
                MATTER
              </Text> */}
                </View>
              </View>
              <View flex={0.5} style={{ justifyContent: "center" }}>
                <Text color="#000" font="rubikMedium">
                  Enter Your Community and See What’s Good Today
                </Text>
              </View>
              <View flex style={{ justifyContent: "space-around" }}>
                <FloatingLabelInput
                  label="Email"
                  value={email}
                  onChangeText={(value) => setEmail(value)}
                  containerStyles={{
                    borderWidth: 0,
                    borderBottomWidth: 1,
                    borderBottomColor: "rgba(0,0,0,.9)",
                  }}
                  labelStyles={{ color: "#000" }}
                  inputStyles={{ color: "#000" }}
                />
                <View>
                  <FloatingLabelInput
                    label="Password"
                    value={password}
                    isPassword={true}
                    onChangeText={(value) => setPassword(value)}
                    containerStyles={{
                      borderWidth: 0,
                      borderBottomWidth: 1,
                      borderBottomColor: "rgba(0,0,0,.9)",
                    }}
                    labelStyles={{ color: "#000" }}
                    inputStyles={{ color: "#000" }}
                    showPasswordImageStyles={{ tintColor: "#000" }}
                  />
                  <Text
                    size="tiny"
                    font="rubikMedium"
                    style={{ position: "absolute", right: 40, bottom: 18 }}
                  >
                    Forgot Password?
                  </Text>
                </View>
              </View>
              <View flex={0.8} style={{ justifyContent: "center" }}>
                <View row style={{ paddingBottom: 20 }}>
                  <View flex middle>
                    <Button
                      loading={loading}
                      color="#000"
                      round
                      shadowless
                      onPress={
                        () => (login({ email, password }), setLoading(true))
                        // AsyncStorage.setItem("user", "user exist", () =>
                        //   dispatch({
                        //     type: "SET_CURRENT_USER",
                        //     payload: { Name: "Eyo John" },
                        //   })
                        // )
                      }
                    >
                      Sign In
                    </Button>
                  </View>
                  <View flex row middle>
                    <View
                      middle
                      style={{
                        width: Layout["dim"](12, "w"),
                        height: Layout["dim"](12, "w"),
                        borderRadius: Layout["dim"](6, "w"),
                        marginRight: 5,
                      }}
                    >
                      <Entypo
                        name="facebook-with-circle"
                        size={Layout["dim"](12, "w")}
                      />
                    </View>
                    <View
                      middle
                      style={{
                        width: Layout["dim"](12, "w"),
                        height: Layout["dim"](12, "w"),
                        borderRadius: Layout["dim"](6, "w"),
                        padding: 0,
                      }}
                      backgroundColor="#000"
                    >
                      <FontAwesome5
                        name="google"
                        size={Layout["dim"](6, "w")}
                        color="rgba(242,234,39,.75)2"
                      />
                    </View>
                  </View>
                </View>
                <Text
                  onPress={() => navigation.navigate("SignUp")}
                  font="rubikMedium"
                  size="tiny"
                >
                  Dont have an account?{" "}
                  <Text size="small" color="#fff">
                    Join our community
                  </Text>
                </Text>
              </View>
            </View>
          </ImageBackground>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    </View>
  );
};
const styles = StyleSheet.create({});
const mapStateToProps = (state) => ({
  errors: state.errors,
  auth: state.auth,
  generic: state.generic,
});

export default connect(mapStateToProps, {
  login,
  resetState,
})(SignIn);
