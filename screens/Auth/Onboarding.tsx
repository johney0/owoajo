import React, { Fragment, useRef, useState } from "react";
// import { LinearGradient } from "expo-linear-gradient";
import { Image, FlatList, Dimensions, ImageBackground } from "react-native";
import { View, Text, Button, useThemeColor } from "../../components/Themed";
import Touchable from "react-native-platform-touchable";
import { Ionicons, Octicons } from "@expo/vector-icons";

import AsyncStorage from "@react-native-community/async-storage";
import { useDispatch } from "react-redux";
import Layout from "../../constants/Layout";
const { width, height } = Dimensions.get("screen");

interface IndexProps {
  navigation: any;
}

const Index: React.FC<IndexProps> = ({ navigation }) => {
  const dispatch = useDispatch();
  const completOnboarding = async () => {
    console.log("done");
    await AsyncStorage.setItem("hasOnboarded", JSON.stringify(true), () =>
      dispatch({
        type: "SET_ONBOARDED",
        payload: true,
      })
    );
  };
  const [Index, setIndex] = useState(0);

  const onViewRef = useRef(({ viewableItems }) => {
    viewableItems.length > 0 &&
      viewableItems &&
      setIndex(viewableItems[0].index);
  });
  const viewConfigRef = useRef({ viewAreaCoveragePercentThreshold: 100 });
  const flatListRef = useRef<any>(null);
  return (
    <View flex>
      <ImageBackground
        source={pages[Index].image}
        style={{
          width,
          height,
        }}
        imageStyle={{ resizeMode: "cover" }}
      >
        <View flex backgroundColor="rgba(0,0,0,.75)">
          <FlatList
            ref={flatListRef}
            scrollEnabled={true}
            onViewableItemsChanged={onViewRef.current}
            viewabilityConfig={viewConfigRef.current}
            data={pages}
            snapToInterval={width}
            horizontal
            onEndReached={() => console.log("end")}
            renderItem={({ item, index }) => {
              return (
                <View
                  key={index}
                  flex
                  style={{
                    justifyContent: "flex-end",
                    width,
                    padding: 20,
                  }}
                >
                  <Text
                    bold
                    font="rubikBold"
                    color="#fff"
                    size={24}
                    style={{ fontWeight: "bold" }}
                  >
                    {item.title}
                  </Text>
                  <Text
                    font="rubikMedium"
                    color="#fff"
                    size={14}
                    style={{ paddingTop: 20, textAlign: "justify" }}
                  >
                    {item.subtitle}
                  </Text>
                </View>
              );
            }}
            keyExtractor={(item) => item.title}
            showsHorizontalScrollIndicator={false}
            initialNumToRender={1}
          />
          <View flex={0.5} style={{ justifyContent: "center" }}>
            <View row style={{ paddingHorizontal: Layout["dim"](8, "w") }}>
              <View flex>
                {Index > 0 && (
                  <Button
                    style={{
                      width: Layout["dim"](14, "w") + 5,
                      height: Layout["dim"](14, "w") + 5,
                      marginHorizontal: Layout["dim"](2, "w"),
                      borderRadius: (Layout["dim"](16, "w") + 5) / 3.5,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                    color="black"
                    shadowColor="rgba(24,119,42,.3)"
                    onPress={() =>
                      Index > 0 &&
                      flatListRef.current.scrollToIndex({
                        animated: true,
                        index: Index - 1,
                      })
                    }
                  >
                    <Ionicons color="#f5f5f5" name="ios-arrow-back" size={20} />
                  </Button>
                )}
              </View>
              <View flex style={{ alignItems: "flex-end" }}>
                <Button
                  style={{
                    width:
                      Index === 2
                        ? Layout["dim"](44, "w") + 5
                        : Layout["dim"](14, "w") + 5,
                    height: Layout["dim"](14, "w") + 5,
                    marginHorizontal: Layout["dim"](2, "w"),
                    borderRadius: (Layout["dim"](16, "w") + 5) / 3.5,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                  color="rgba(242,234,39,1)"
                  shadowColor="rgba(24,119,42,.3)"
                  onPress={() =>
                    Index + 1 < pages.length
                      ? flatListRef.current.scrollToIndex({
                          animated: true,
                          index: Index + 1,
                        })
                      : completOnboarding()
                  }
                >
                  {Index === 2 ? (
                    <Text size={18} style={{ color: "#000" }}>
                      Let's Do This!
                    </Text>
                  ) : (
                    <Ionicons color="#000" name="ios-arrow-forward" size={20} />
                  )}
                </Button>
              </View>
            </View>
          </View>
        </View>
      </ImageBackground>
      {/* <View row middle>
          {pages.map((page, index) => {
            return (
              <Octicons
                key={index}
                name="primitive-dot"
                size={30}
                style={{ paddingHorizontal: 2.5 }}
                color={index === Index ? "#5052D9" : "#E1DDF8"}
              />
            );
          })}
        </View> */}
    </View>
  );
};
export default Index;

const pages = [
  {
    backgroundColor: "transparent",
    image: require("../../assets/images/onboard1.jpg"),
    title: "Welcome",
    subtitle:
      "Thank you for downloading Black Pennies Matter, presented by OwoAjo.  By signing up, you’re joining a community dedicated to the financial liberation and self-determination of Black people!",
  },
  {
    backgroundColor: "transparent",
    image: require("../../assets/images/onboard2.jpg"),
    title: "What you get",
    subtitle:
      "Every day, we will ask you one simple question: Do Black Pennies Matter?  When you say yes (and you are over 18 years of age), you send one cent to our growing community of Black Pennies which will be used to improve the lives of Black people in America and beyond!",
  },
  {
    backgroundColor: "transparent",
    image: require("../../assets/images/onboard3.jpg"),
    title: "Get started",
    subtitle: "Let’s show the world Black Pennies Matter!”",
  },
];
