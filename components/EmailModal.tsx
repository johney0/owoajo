import React, { useEffect, useState, useRef } from "react";
import { Alert, StyleSheet, Dimensions } from "react-native";
import { View, Text, Button, useThemeColor } from "../components/Themed";
import Modal from "react-native-modal";
import Layout from "../constants/Layout";
import Touchable from "react-native-platform-touchable";

import { Ionicons } from "@expo/vector-icons";
import { CodeField, Cursor } from "react-native-confirmation-code-field";
import { useDispatch, useSelector } from "react-redux";
import {
  verifyEmail,
  successLoginFunction,
} from "../redux/actions/authentication";
import Navigation from "../navigation";
import { useNavigation } from "@react-navigation/native";
import { ScrollView } from "react-native-gesture-handler";
const { width } = Dimensions.get("screen");
interface EmailModalProps {
  emailModal: boolean;
  setEmailModal: (val: boolean) => void;
  email: string;
}

const EmailModal: React.FC<EmailModalProps> = ({
  emailModal,
  setEmailModal,
  email,
}) => {
  const [pin, setPin] = useState("");
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const [height, setHeight] = useState(40);
  const verifyError: any = useSelector(
    (state: {
      errors: {
        Verify?: object;
      };
    }) => state.errors.Verify
  );
  const scroll = useRef<any>(null);

  return (
    <Modal
      propagateSwipe
      avoidKeyboard
      isVisible={emailModal}
      style={{
        justifyContent: "flex-end",
        margin: 0,
        backgroundColor: "rgba(0,0,0,.2)",
      }}
      onSwipeComplete={() => (setEmailModal(false), setHeight(40))}
      swipeDirection={["down"]}
      onBackdropPress={() => (setEmailModal(false), setHeight(40))}
      animationInTiming={800}
      animationOutTiming={800}
    >
      <View
        height={height}
        width={100}
        backgroundColor="#F2EA27"
        style={{
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          justifyContent: "space-around",
        }}
      >
        <ScrollView
          horizontal
          snapToInterval={width}
          scrollEventThrottle={10}
          ref={scroll}
          scrollEnabled={false}
          showsHorizontalScrollIndicator={false}
        >
          <View
            flex
            width={100}
            style={{ padding: 20, alignItems: "center" }}
            space="around"
          >
            <Text>Securing Our Community</Text>
            <Text center>
              Black Pennies Matter is committed to your safety. We want to make
              sure everyone who is here is who they say they are. We do this by
              verifying email.
            </Text>
            <Text center>
              We sent you a 5 digit PIN. Please check your email and when you
              get the PIN, tap here to finish signing up.
            </Text>
            <Button
              shadowless
              style={{
                height: 44,
                padding: 0,
                justifyContent: "center",
                alignItems: "center",
              }}
              color="#14172C"
              onPress={() => (
                scroll.current.scrollTo({ x: width }), setHeight(30)
              )}
            >
              I'm Down
            </Button>
          </View>
          <View width={100} style={{ padding: 20 }}>
            <Text font="rubikMedium" size="small" color="#14172C">
              Enter the pin sent to your email{" "}
              <Text size="tiny">({email})</Text>
            </Text>
            <View
              flex
              middle
              style={{
                alignSelf: "stretch",
              }}
            >
              <CodeField
                value={pin}
                onChangeText={(pin) => setPin(pin)}
                cellCount={5}
                keyboardType="number-pad"
                renderCell={({ index, symbol, isFocused }) => {
                  const hasValue = Boolean(symbol);

                  return (
                    <View
                      key={index}
                      middle
                      style={[
                        {
                          marginHorizontal: Layout["dim"](3, "w"),
                          borderBottomWidth: 2,
                          borderColor: "#14172C",
                          fontFamily: "nunito-bold",
                          width: Layout["dim"](10, "w"),
                        },
                      ]}
                    >
                      <Text
                        color="#14172C"
                        size="big"
                        style={[
                          {
                            width: Layout["dim"](12, "w"),
                            height: Layout["dim"](5, "h"),
                            lineHeight: Layout["dim"](5, "h"),
                            textAlign: "center",
                          },
                        ]}
                      >
                        {symbol || (isFocused ? <Cursor /> : null)}
                      </Text>
                    </View>
                  );
                }}
              />
            </View>
            <Text
              color="#14172C"
              font="rubikMedium"
              style={{ alignSelf: "center" }}
            >
              Resend Pin <Ionicons name="md-refresh" />
            </Text>
            <View flex style={{ justifyContent: "center" }}>
              <Button
                loading={loading}
                shadowless
                onPress={() => (
                  setLoading(true),
                  dispatch(
                    verifyEmail(
                      { email, pin },
                      (result) => (
                        setLoading(false),
                        //Route to new screens
                        result.success
                          ? Alert.alert(
                              "Registered Succesfully",
                              "Welcome",
                              [
                                {
                                  text: "OK",
                                  onPress: () => (
                                    dispatch({
                                      type: "SET_NEW_USER",
                                      payload: true,
                                    }),
                                    dispatch(successLoginFunction(result))
                                  ),
                                },
                              ],
                              { cancelable: false }
                            )
                          : Alert.alert("Registeration Failed", result.error)
                      )
                    )
                  )
                )}
                style={{
                  alignSelf: "flex-end",
                  width: 44,
                  height: 44,
                  borderRadius: 22,
                  padding: 0,
                  justifyContent: "center",
                  alignItems: "center",
                }}
                color="#14172C"
              >
                <Ionicons name="ios-arrow-forward" size={22} color="#F2EA27" />
              </Button>
            </View>
          </View>
        </ScrollView>
      </View>
    </Modal>
  );
};
export default EmailModal;
