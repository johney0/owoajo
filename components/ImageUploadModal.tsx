import React from "react";
import { StyleSheet, Dimensions } from "react-native";
import Modal from "react-native-modal";
import { Ionicons, AntDesign } from "@expo/vector-icons";
import Touchable from "react-native-platform-touchable";
import Layout from "../constants/Layout";
import { View, Text, useThemeColor, Button } from "../components/Themed";

const { width, height } = Dimensions.get("screen");

interface ImageUploadModalProps {
  open: boolean;
  setOpen: (val: boolean) => void;
  callBack: (val: string) => void;
}

const ImageUploadModal: React.FC<ImageUploadModalProps> = ({
  open,
  setOpen,
  callBack,
}) => {
  return (
    <Modal
      propagateSwipe
      isVisible={open}
      style={{
        justifyContent: "flex-end",
        margin: 0,
        backgroundColor: "rgba(0,0,0,.2)",
      }}
      onSwipeComplete={() => setOpen(false)}
      swipeDirection={["down"]}
      onBackdropPress={() => setOpen(false)}
      animationInTiming={800}
      animationOutTiming={800}
    >
      <View
        style={{
          backgroundColor: "white",
          width,
          height: Layout["dim"](20, "h"),
          borderTopRightRadius: Layout["dim"](5.33, "w"),
          borderTopLeftRadius: Layout["dim"](5.33, "w"),
        }}
        center
      >
        <View
          flex={0.1}
          style={{
            borderTopWidth: 2,
            borderTopColor: "lightgray",
            width: Layout["dim"](10, "w"),
            marginTop: Layout["dim"](2, "h"),
          }}
        />
        <View
          flex={0.7}
          style={{
            paddingHorizontal: Layout["dim"](10, "w"),
            alignSelf: "flex-start",
            alignItems: "flex-start",
          }}
        >
          <Text
            size={"big"}
            color="#ddd"
            style={{
              fontFamily: "rubikBold",
              textAlign: "left",
              alignSelf: "flex-start",
            }}
          >
            Upload From?
          </Text>
        </View>
        <View flex row middle style={{ width }}>
          <Touchable style={{ flex: 1 }} onPress={() => callBack("Camera")}>
            <View
              flex
              row
              space="around"
              center
              style={{ alignSelf: "stretch" }}
            >
              <Ionicons name="ios-camera" size={24} color="rgb(242,234,39)" />
              <Text
                size={"small"}
                color={"primary"}
                style={{ fontFamily: "rubikBold" }}
              >
                Camera
              </Text>
            </View>
          </Touchable>
          <Touchable style={{ flex: 1 }} onPress={() => callBack("Media")}>
            <View
              flex
              row
              center
              space="around"
              backgroundColor="primary"
              style={{
                alignSelf: "stretch",
                borderTopLeftRadius: 20,
              }}
            >
              <Text
                size={"small"}
                color="#fff"
                style={{ fontFamily: "rubikBold" }}
              >
                Gallery
              </Text>
              <Ionicons name="md-image" size={24} color="#fff" />
            </View>
          </Touchable>
        </View>
      </View>
    </Modal>
  );
};
export default ImageUploadModal;
