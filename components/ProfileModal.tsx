import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  ImageBackground,
  TextInput,
  Image,
  Alert,
  Dimensions,
} from "react-native";
import Modal from "react-native-modal";
import { View, Text, useThemeColor, Button } from "../components/Themed";
import { Ionicons, AntDesign, FontAwesome } from "@expo/vector-icons";
import Layout from "../constants/Layout";
import { PaymentsStripe as Stripe } from "expo-payments-stripe";
import { addCard } from "../redux/actions";
import { useDispatch } from "react-redux";
import { updateProfile } from "../redux/actions/settings";
import { wait, _pickImage } from "./Functions";
import ImageUploadModal from "./ImageUploadModal";
import Touchable from "react-native-platform-touchable";
const { width } = Dimensions.get("screen");
interface ProfileModalProps {
  Profile: {};
  profileModal: boolean;
  setProfileModal: (val: boolean) => void;
}

const ProfileModal: React.FC<ProfileModalProps> = ({
  Profile,
  profileModal,
  setProfileModal,
}) => {
  const [name, setName] = useState("");
  const [amount, setAmount] = useState("");
  const [frequency, setFrequency] = useState("");
  const [ethnicity, setEthnicity] = useState("");
  const [gender, setGender] = useState("");
  const [dob, setDob] = useState("");
  const [avatar, setAvatar] = useState("");
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const [focus, setFocus] = useState("");
  const dispatch = useDispatch();
  const updatingProfile = async () => {
    const data = {
      fullName: name,
      donationAmount: amount,
      frequency,
      ethnicity,
      gender,
      dob,
    };
    dispatch(updateProfile(data));
  };
  const [avatarFlip, setAvatarFlip] = useState(false);

  const uploadImage = (val) =>
    _pickImage(val, (data) => (setAvatarFlip(true), console.log(data)));
  return (
    <Modal
      avoidKeyboard
      isVisible={profileModal}
      style={{
        justifyContent: "flex-end",
        margin: 0,
        backgroundColor: "rgba(0,0,0,.2)",
      }}
      onSwipeComplete={() => (setProfileModal(false), setLoading(false))}
      swipeDirection={["down"]}
      onBackdropPress={() => (setProfileModal(false), setLoading(false))}
      animationInTiming={800}
      animationOutTiming={800}
    >
      <View
        backgroundColor="#f5f5f5"
        width={100}
        height={35}
        style={{
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
      >
        <ImageBackground
          source={require("../assets/images/modal.jpg")}
          style={{
            width: "100%",
            height: "100%",
          }}
          imageStyle={{ borderTopLeftRadius: 20, borderTopRightRadius: 20 }}
        >
          <View
            flex
            backgroundColor="rgba(0,0,0,.75)"
            style={{
              padding: 20,
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
            }}
          >
            <View
              flex={0.5}
              row
              space="between"
              style={{ alignItems: "center" }}
            >
              <View flex>
                <Text size="small" color="primary" font="rubikMedium">
                  Update your profile
                </Text>
                <Text size="tiny" color="grey" font="rubik">
                  Fill in the fields you wish to update below
                </Text>
              </View>
              <View flex={0.3} middle>
                <Touchable onPress={() => setOpen(true)}>
                  <View
                    backgroundColor="black"
                    style={{ width: 50, height: 50, borderRadius: 25 }}
                    middle
                  >
                    <Ionicons
                      name="ios-person"
                      color={useThemeColor({}, "primary")}
                      size={25}
                    />
                  </View>
                </Touchable>
              </View>
            </View>
            <View flex>
              <View flex row style={{ alignItems: "center" }}>
                <View flex style={{ justifyContent: "center" }}>
                  <Text size="tiny" color="grey" font="rubikMedium">
                    ENTER FULL NAME
                  </Text>
                  <TextInput
                    caretHidden
                    value={name}
                    onChangeText={(val) => setName(val)}
                    autoFocus
                    onFocus={() => setFocus("name")}
                    style={{
                      fontFamily: "rubikMedium",
                      marginTop: 4,
                      fontSize: Layout["dim"](2, "h"),
                      borderBottomColor:
                        focus === "name" ? "rgb(242,234,39)" : "grey",
                      borderBottomWidth: 1,
                      color: useThemeColor({}, "grey"),
                    }}
                  />
                </View>
                <View flex={0.3}></View>
              </View>
              {/* <View flex row style={{ alignItems: "center" }}>
            <View flex style={{ justifyContent: "center" }}>
              <Text size="tiny" color="grey" font="rubikMedium">
                DONATING AMOUNT
              </Text>
              <View style={{ marginTop: 4 }} row>
                <Text
                  style={{
                    fontFamily: "rubikMedium",
                    fontSize: Layout["dim"](2, "h"),
                    letterSpacing: 5,
                  }}
                >
                  $
                </Text>
                <TextInput
                  value={amount}
                  onChangeText={(amount) => setAmount(amount)}
                  style={{
                    fontFamily: "rubikMedium",
                    fontSize: Layout["dim"](2, "h"),
                    letterSpacing: 5,
                    flex: 1,
                  }}
                />
              </View>
            </View>
            <View flex={0.5} style={{ justifyContent: "center" }}>
              <Text size="tiny" color="grey" font="rubikMedium">
                DONATING FREQUENCY
              </Text>
              <View style={{ marginTop: 4 }} row>
                <TextInput
                  value={frequency}
                  onChangeText={(frequency) => setFrequency(frequency)}
                  style={{
                    fontFamily: "rubikMedium",
                    fontSize: Layout["dim"](2, "h"),
                    letterSpacing: 5,
                    flex: 1,
                  }}
                />
                <Text
                  style={{
                    fontFamily: "rubikMedium",
                    fontSize: Layout["dim"](2, "h"),
                    letterSpacing: 5,
                    flex: 1,
                  }}
                >
                  /day
                </Text>
              </View>
            </View>
          </View>
          */}
              <View flex row style={{ alignItems: "center" }}>
                <View style={{ width: width * 0.2 }}>
                  <Text size="tiny" color="grey" font="rubikMedium">
                    ETHNICITY
                  </Text>
                  <View style={{ alignItems: "center" }}>
                    <TextInput
                      value={ethnicity}
                      onFocus={() => setFocus("ethnic")}
                      onChangeText={(val) => setEthnicity(val)}
                      caretHidden
                      style={{
                        fontFamily: "rubikMedium",
                        marginTop: 4,
                        fontSize: Layout["dim"](2, "h"),
                        width: "100%",
                        borderBottomColor:
                          focus === "ethnic" ? "rgb(242,234,39)" : "grey",
                        borderBottomWidth: 1,
                        color: useThemeColor({}, "grey"),
                      }}
                    />
                  </View>
                </View>
                <View style={{ marginLeft: 50, width: width * 0.2 }}>
                  <Text size="tiny" color="grey" font="rubikMedium">
                    GENDER
                  </Text>
                  <View style={{ alignItems: "center" }}>
                    <TextInput
                      caretHidden
                      value={gender}
                      onFocus={() => setFocus("gender")}
                      onChangeText={(val) => setGender(val)}
                      style={{
                        fontFamily: "rubikMedium",
                        marginTop: 4,
                        fontSize: Layout["dim"](2, "h"),
                        width: "100%",
                        borderBottomColor:
                          focus === "gender" ? "rgb(242,234,39)" : "grey",
                        borderBottomWidth: 1,
                        color: useThemeColor({}, "grey"),
                      }}
                    />
                  </View>
                </View>
                <View style={{ marginLeft: 50, width: width * 0.2 }}>
                  <Text size="tiny" color="grey" font="rubikMedium">
                    DOB
                  </Text>
                  <View style={{ alignItems: "center" }}>
                    <TextInput
                      caretHidden
                      value={dob}
                      onChangeText={(val) => setDob(val)}
                      onFocus={() => setFocus("dob")}
                      style={{
                        fontFamily: "rubikMedium",
                        marginTop: 4,
                        fontSize: Layout["dim"](2, "h"),
                        width: "100%",
                        borderBottomColor:
                          focus === "dob" ? "rgb(242,234,39)" : "grey",
                        borderBottomWidth: 1,
                        color: useThemeColor({}, "grey"),
                      }}
                    />
                  </View>
                </View>
              </View>
            </View>
            <View flex={0.5} middle>
              <Button
                loading={loading}
                shadowless
                style={{ width: "80%" }}
                color="black"
                onPress={() => updatingProfile()}
              >
                Update Profile
              </Button>
            </View>
          </View>
        </ImageBackground>
        <ImageUploadModal
          open={open}
          setOpen={(val) => setOpen(val)}
          callBack={(val) => (
            setOpen(false), wait(1000).then(() => uploadImage(val))
          )}
        />
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({});
export default ProfileModal;
