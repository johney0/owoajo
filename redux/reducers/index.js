import { combineReducers } from "redux";
import errorReducers from "./errorReducers";
import authReducers from "./authReducers";
import genericReducer from "./genericReducer";
import settingsReducer from "./settingsReducer";

export default combineReducers({
  errors: errorReducers,
  auth: authReducers,

  generic: genericReducer,
  settings: settingsReducer,
});
