import {
  SET_CARD,
  DONATIONS,
  SET_PROFILE,
  ADD_DONATION,
  SET_QUOTE_READ,
  SET_NEW_USER,
} from "../actions/types";

const initialState = {
  Profile: {
    card: {},
    email: "",
    donationAmount: 0,
    phone: "",
    totalNumberOfDonations: 0,
    totalAmountDonated: 0,
    QuoteRead: false,
    newUser: false,
  },
  donations: [],
};
export default function (state = initialState, action) {
  switch (action.type) {
    case SET_PROFILE:
      return {
        ...state,
        Profile: action.payload,
      };
    case SET_CARD:
      return {
        ...state,
        card: action.payload,
      };
    case DONATIONS:
      return {
        ...state,
        donations: action.payload,
      };
    case ADD_DONATION:
      return {
        ...state,
        donations: [...state.donations, action.payload],
      };
    case SET_QUOTE_READ:
      return {
        ...state,
        QuoteRead: action.payload,
      };
    case SET_NEW_USER:
      return {
        ...state,
        newUser: action.payload,
      };

    default:
      return state;
  }
}
