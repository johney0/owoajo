import {
  SUCCESS,
  GET_ERRORS,
  ENTER_AUTH,
  GEO_LOCATION,
  RESET_SUCCESS,
  RESET_ERRORS,
} from "./types";

export const setSuccess = (value, action) => (dispatch) => {
  console.log("hitting success", value, action);
  dispatch({
    type: SUCCESS,
    payload: {
      action,
      value,
    },
  });
};

export const setErrors = (value, action) => (dispatch) => {
  console.log("errors", value, action);

  dispatch({
    type: GET_ERRORS,
    payload: { action, value },
  });
};

export const setSendStatus = (value) => (dispatch) => {
  if (value !== "") {
    console.log("send dnar status", value);
  }
  dispatch({
    type: SEND_STATUS,
    payload: value,
  });
};

export const setScannedValue = (data) => (dispatch) => {
  dispatch({
    type: SET_SCANNED_VALUE,
    payload: data,
  });
};

export const enterAuth = (data) => (dispatch) => {
  console.log("hitting here");
  dispatch({
    type: ENTER_AUTH,
    payload: data,
  });
};

export const verifiedAuth = (data) => (dispatch) => {
  console.log("data", data);
  dispatch({
    type: VERIFIED,
    payload: data,
  });
};

export const fetched = (data) => (dispatch) => {
  console.log("data", data);
  dispatch({
    type: FETCHED,
    payload: data,
  });
};

export const setFingerEnroll = (val) => (dispatch) => {
  console.log("hit", val);
  // dispatch({
  //   type: FINGER_ENROLLED,
  //   payload: val
  // });
};

export const resetState = () => (dispatch) => {
  console.log("resetting");
  dispatch({
    type: RESET_ERRORS,
    payload: {
      AddCard: {},
      Send: {},
      Deposit: {},
      QrScan: {},
      QrPay: {},
      AddMomo: {},
      SignIn: {},
      SignUp: {},
      MomoAdd: {},
      Username: {},
      changePasscode: {},
      emailChange: {},
      walletBalance: {},
      cedisTransactions: "",
      ForgotPassword: {},
      TransactionsDownload: "",
      GetMomo: "",
    },
  });
  dispatch({
    type: RESET_SUCCESS,
    payload: {
      Send: "",
      Deposit: false,
      QrScan: {},
      QrPay: false,
      SignIn: "",
      SignUp: "",
      MomoAdd: "",
      Username: "",
      changePasscode: {},
      emailChange: "",
      ForgotPassword: {},
      NewCard: false,
      TransactionsDownload: [],
    },
  });
  dispatch(enterAuth(""));
};

export const setGlobalLocation = (data) => (dispatch) => {
  dispatch({
    type: GEO_LOCATION,
    payload: data,
  });
};
