import setAuthToken from "../setAuthToken";
import axios from "axios";
import { Alert } from "react-native";
import { setErrors } from "./generic";
import { DONATIONS, ADD_DONATION } from "./types";
import { getProfile } from "./settings";
const URL = "https://owoajo-staging.herokuapp.com";

export const addCard = (data, callback) => (dispatch) => {
  setAuthToken();
  axios
    .post(`${URL}/users/me/save-card`, { data })
    .then((res) => (callback(res.data), dispatch(getProfile())))
    .catch((err) => callback(err.response.data));
};

export const donate = (callback) => (dispatch) => {
  setAuthToken();
  axios
    .post(`${URL}/users/me/donate`)
    .then(
      (res) => (
        dispatch({ type: ADD_DONATION, payload: res.data.data }),
        callback(res.data)
      )
    )
    .catch((err) => callback(err.response.data));
};

export const getDonations = () => (dispatch) => {
  setAuthToken();
  axios
    .get(`${URL}/users/me/donations`)
    .then((res) =>
      dispatch({
        type: DONATIONS,
        payload: res.data.data,
      })
    )
    .catch((err) => console.log("error getting donations", err.response.data));
};
