import axios from "axios";
import { SET_CURRENT_USER, LOG_OUT } from "./types";
import jwt_decode from "jwt-decode";
import AsyncStorage from "@react-native-community/async-storage";
import {
  setErrors,
  enterAuth,
  verifiedAuth,
  setSuccess,
  resetState,
} from "./generic";
const URL = "https://owoajo-staging.herokuapp.com";
export const verifyEmail = (data, callback) => (dispatch) => {
  axios
    .post(`${URL}/auth/user/verify`, data)
    .then((res) => callback(res.data))
    .catch((err) => callback(err.response.data));
};

export const login = (data, callback) => async (dispatch) => {
  axios
    .post(`${URL}/auth/user/login`, data)
    .then((res) => dispatch(successLoginFunction(res.data)))
    .catch((err) => dispatch(errorLoginFunction(err)));
};

export const successLoginFunction = (data) => async (dispatch) => {
  const { token } = data;
  await AsyncStorage.setItem("user", token, () => {
    dispatch(setSuccess("succesful login", "SignIn"));
    dispatch(setCurrentUser(token));
  });
};

const errorLoginFunction = (err) => (dispatch) => {
  if (err.message === "Network Error") {
    dispatch(setErrors({ message: "Not Connected" }, "SignIn"));
  } else {
    err !== undefined && dispatch(setErrors(err.response.data, "SignIn"));
  }
};

export const register = (data) => (dispatch) => {
  axios
    .post(`${URL}/auth/user/register`, data)
    .then((res) => {
      if (!res.data.success) {
        dispatch(setErrors(res.data.data));
      }
      dispatch(setSuccess({ message: "succesful Sign Up" }, "SignUp"));
    })
    .catch((err) => {
      if (err.message === "Network Error") {
        dispatch(setErrors({ message: "Not Connected" }));
      } else {
        err !== undefined && dispatch(setErrors(err.response.data, "SignUp"));
      }
    });
};

export const setCurrentUser = (token) => (dispatch) => {
  const decoded = jwt_decode(token);
  dispatch({
    type: SET_CURRENT_USER,
    payload: decoded,
  });
};

export const logout = () => async (dispatch) => {
  console.log("logging out");
  await AsyncStorage.removeItem("user", () => dispatch({ type: LOG_OUT }));
  resetState();
};
