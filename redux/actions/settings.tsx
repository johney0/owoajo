import axios from "axios";
import { SET_CARD, SET_PROFILE } from "./types";
import setAuthToken from "../setAuthToken";
import { setErrors, setSuccess, enterAuth, verifiedAuth } from "./generic";
import { Alert } from "react-native";
const URL = "https://owoajo-staging.herokuapp.com";

export const getProfile = () => (dispatch) => {
  setAuthToken();
  axios
    .get(`${URL}/users/me/profile`)
    .then((res) =>
      dispatch({
        type: SET_PROFILE,
        payload: res.data.data,
      })
    )
    .catch((err) => console.log("err", err.response.data));
};

export const updateProfile = (data) => (dispatch) => {
  setAuthToken();
  axios
    .patch(`${URL}/users/me/profile`, data)
    .then(
      (res) => (
        Alert.alert("Updated profile", "Succesfully updated profile data"),
        dispatch({
          type: SET_PROFILE,
          payload: res.data.data,
        })
      )
    )
    .catch((err) =>
      Alert.alert("Error updating profile", err.response.data.error)
    );
};
