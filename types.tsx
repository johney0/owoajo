export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  TabOne: undefined;
  TabTwo: undefined;
};

export type MainStackParamList = {
  Auth: AuthStackParamList;
  App: AppStackParamList;
  Onboarding: undefined;
  NotFound: undefined;
};
export type AuthStackParamList = {
  SignUp: undefined;
  EmailAuth: undefined;
  ForgotPassword: undefined;
  ForgotPin: undefined;
  SignIn: undefined;
  NewPassword: undefined;
  Kyc: undefined;
};
export type AppStackParamList = { Home: undefined };

export type TabOneParamList = {
  TabOneScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};
