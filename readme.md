react-native/Libraries/Image/RCTUIImageViewAnimated.m

Lines 283 to 289

- (void)displayLayer:(CALayer \*)layer
  {
  if (\_currentFrame) {
  layer.contentsScale = self.animatedImageScale;
  layer.contents = (\_\_bridge id)\_currentFrame.CGImage;
  } else {
  [super displayLayer:layer];
  }
  }
