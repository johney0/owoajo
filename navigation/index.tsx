import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";

import React, { Fragment, useEffect } from "react";
import { ColorSchemeName } from "react-native";

import NotFoundScreen from "../screens/NotFoundScreen";
import {
  RootStackParamList,
  MainStackParamList,
  AuthStackParamList,
} from "../types";
import Home from "../screens/Home";
import LinkingConfiguration from "./LinkingConfiguration";
import AsyncStorage from "@react-native-community/async-storage";
import DrawerContent from "./DrawerContent";
import { useSelector, useDispatch, TypedUseSelectorHook } from "react-redux";
import { isEmptyObj } from "../components/Functions";
import SignUp from "../screens/Auth/SignUp";
import SignIn from "../screens/Auth/SignIn";
import OnboardingScreen from "../screens/Auth/Onboarding";
import Profile from "../screens/Profile";
import Support from "../screens/Support";
import Help from "../screens/Help";
import Settings from "../screens/Settings";
import History from "../screens/History";
import QuoteScreen from "../screens/QuoteScreen";
import FactScreen from "../screens/FactScreen";
import QuestionScreen from "../screens/QuestionScreen";
import NewUserScreen from "../screens/NewUserScreen";
import LaterScreen from "../screens/ComeBackLater";
const Drawer = createDrawerNavigator();
const Quote = createStackNavigator();
const NewUser = createStackNavigator();

export default function Navigation({
  colorScheme,
}: {
  colorScheme: ColorSchemeName;
}) {
  const user: any = useSelector(
    (state: { auth: { User: {} } }) => state.auth.User
  );
  const quoteRead: any = useSelector(
    (state: { settings: { QuoteRead: false } }) => state.settings.QuoteRead
  );
  const newUser: any = useSelector(
    (state: { settings: { newUser: false } }) => state.settings.newUser
  );
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
    >
      {user && !isEmptyObj(user) ? (
        newUser ? (
          <NewUser.Navigator screenOptions={{ headerShown: false }}>
            <NewUser.Screen name="NewUserScreen" component={NewUserScreen} />
          </NewUser.Navigator>
        ) : quoteRead ? (
          <Drawer.Navigator
            initialRouteName="App"
            drawerStyle={{
              backgroundColor: "transparent",
              flex: 1,
              justifyContent: "center",
            }}
            drawerContent={(props) => <DrawerContent {...props} />}
          >
            <Drawer.Screen name="Home" component={Home} />
            <Drawer.Screen name="Profile" component={Profile} />
            <Drawer.Screen name="History" component={History} />
            <Drawer.Screen name="Help" component={Help} />
            <Drawer.Screen name="Support" component={Support} />
            <Drawer.Screen name="Settings" component={Settings} />
          </Drawer.Navigator>
        ) : (
          <Quote.Navigator screenOptions={{ headerShown: false }}>
            <Quote.Screen name="QuoteScreen" component={QuoteScreen} />
            <Quote.Screen name="QuestionScreen" component={QuestionScreen} />
            <Quote.Screen name="FactScreen" component={FactScreen} />
            <Quote.Screen name="LaterScreen" component={LaterScreen} />
          </Quote.Navigator>
        )
      ) : (
        <MainNavigator />
      )}
    </NavigationContainer>
  );
}

const Main = createStackNavigator<MainStackParamList>();
function MainNavigator() {
  const onboarded: any = useSelector(
    (state: { auth: { onboarded: boolean } }) => state.auth.onboarded
  );
  const dispatch = useDispatch();
  useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken;
      try {
        userToken = await AsyncStorage.getItem("hasOnboarded");
      } catch (e) {
        // Restoring token failed
      }
      dispatch({
        type: "SET_ONBOARDED",
        payload: userToken,
      });
    };
    bootstrapAsync();
  });
  return (
    <Main.Navigator screenOptions={{ headerShown: false }}>
      {!onboarded ? (
        <Main.Screen name="Onboarding" component={OnboardingScreen} />
      ) : (
        <Main.Screen name="Auth" component={AuthNavigator} />
      )}
    </Main.Navigator>
  );
}

const Auth = createStackNavigator<AuthStackParamList>();
function AuthNavigator() {
  return (
    <Auth.Navigator
      initialRoute="SignIn"
      screenOptions={{ headerShown: false }}
    >
      <Auth.Screen name="SignUp" component={SignUp} />

      <Auth.Screen name="SignIn" component={SignIn} />

      <Auth.Screen name="NotFound" component={NotFoundScreen} />
    </Auth.Navigator>
  );
}
