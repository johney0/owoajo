import React from "react";
import { StyleSheet, Alert, Image } from "react-native";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import { Ionicons, FontAwesome5, Entypo } from "@expo/vector-icons";
import { Text, View, Button, useThemeColor } from "../components/Themed";
import AsyncStorage from "@react-native-community/async-storage";
import { useDispatch, useSelector } from "react-redux";
import Layout from "../constants/Layout";
import Touchable from "react-native-platform-touchable";

interface DrawerContentProps {
  navigation: any;
}

const DrawerContent: React.FC<DrawerContentProps> = (props) => {
  const { navigation } = props;
  const dispatch = useDispatch();
  const { fullName, avatar }: any = useSelector(
    (state: {
      auth: {
        User: object;
      };
    }) => state.auth.User
  );
  return (
    <View
      flex
      width={30}
      style={{
        backgroundColor: "#14171a",
      }}
    >
      <View flex style={styles.drawerContent}>
        <View flex={0.4} style={styles.userInfoSection}>
          <Touchable onPress={() => navigation.navigate("Home")}>
            <Image
              source={require("../assets/images/blmFist.png")}
              style={{
                width: Layout["dim"](13, "w"),
                height: Layout["dim"](13, "w"),
              }}
            />
          </Touchable>
        </View>

        <View
          flex={0.15}
          middle
          style={{
            borderBottomWidth: 3,
            borderColor: "rgba(225,225,225,1)",
            width: "60%",
            alignSelf: "center",
            paddingBottom: Layout["dim"](1, "h"),
          }}
        >
          <Touchable onPress={() => navigation.navigate("Profile")}>
            <View
              middle
              style={{
                backgroundColor: "#F2EA27",
                borderRadius: Layout["dim"](2, "w"),
                width: 40,
                height: 40,
              }}
            >
              <Ionicons name="ios-person" color="#14171a" size={20} />
            </View>
          </Touchable>
        </View>
        <View flex style={styles.drawerSection} middle>
          <Touchable onPress={() => navigation.navigate("History")}>
            <View
              middle
              style={{
                borderWidth: 1,
                borderColor: "#F2EA27",
                borderRadius: Layout["dim"](2, "w"),
                width: 40,
                height: 40,
              }}
            >
              <FontAwesome5 name="history" color="#fff" size={20} />
            </View>
          </Touchable>
          <Touchable onPress={() => navigation.navigate("Help")}>
            <View
              middle
              style={{
                borderWidth: 1,
                borderColor: "#F2EA27",
                borderRadius: Layout["dim"](2, "w"),
                width: 40,
                height: 40,
              }}
            >
              <Entypo name="thumbs-up" color="#fff" size={20} />
            </View>
          </Touchable>
          <Touchable onPress={() => navigation.navigate("Support")}>
            <View
              middle
              style={{
                borderRadius: Layout["dim"](2, "w"),
                width: 40,
                height: 40,
                borderWidth: 1,
                borderColor: "#F2EA27",
              }}
            >
              <Entypo name="chat" color="#fff" size={20} />
            </View>
          </Touchable>
          <Touchable onPress={() => navigation.navigate("Settings")}>
            <View
              middle
              style={{
                borderWidth: 1,
                borderColor: "#F2EA27",
                borderRadius: Layout["dim"](2, "w"),
                width: 40,
                height: 40,
              }}
            >
              <Ionicons name="ios-settings" color="#fff" size={20} />
            </View>
          </Touchable>
        </View>
        <View flex={0.3} middle>
          <Touchable
            onPress={() => {
              AsyncStorage.removeItem("user", () => {
                dispatch({
                  type: "LOG_OUT",
                });
              });
            }}
          >
            <View
              middle
              style={{
                backgroundColor: "#F2EA27",
                borderRadius: 20,
                width: 40,
                height: 40,
                paddingLeft: 4,
              }}
            >
              <Ionicons name="ios-log-out" color="#14171a" size={20} />
            </View>
          </Touchable>
        </View>
      </View>
    </View>
  );
};
export default DrawerContent;

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 32,
    justifyContent: "center",
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: "bold",
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  section: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 15,
  },
  paragraph: {
    fontWeight: "bold",
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
    flex: 1,
    justifyContent: "space-around",
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: "#f4f4f4",
    borderTopWidth: 1,
    backgroundColor: "green",
  },
  preference: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});
